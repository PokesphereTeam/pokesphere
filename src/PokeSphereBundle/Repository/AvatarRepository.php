<?php
/**
 * Created by PhpStorm.
 * User: Sobraz
 * Date: 05/05/2017
 * Time: 19:49
 */
namespace PokeSphereBundle\Repository;
use GraphAware\Neo4j\OGM\Repository\BaseRepository;
use PokeSphereBundle\Entity\Avatar;
use PokeSphereBundle\Entity\Community;
class AvatarRepository extends BaseRepository
{
    public function getAvalaibleUserAvatars(int $user_id = null){
        $query = "MATCH (a:AvatarUser)-[:GENERATED_BY]->(u:User) 
                    WHERE id(u) = {id_user} RETURN a";
        if($user_id !== null)
            $query.= " UNION MATCH (a:AvatarUser) 
                        WHERE NOT (a)-[:GENERATED_BY]->() RETURN a";
        $query_object = $this->entityManager->createQuery($query);
        $query_object->addEntityMapping("a", Avatar::class);
        if($user_id !== null)
            $query_object->setParameter("id_user",$user_id);
        return $query_object->execute();
    }
    public function getAvalaibleCommunityAvatars(Community $community = null){
        $query = "MATCH (a:AvatarCommunity) WHERE NOT (a)-[:GENERATED_BY]->() RETURN a";
        if($community !== null)
            $query .= " UNION MATCH (c:Community)-[CAN_USE]->(a:AvatarCommunity) 
                                WHERE id(c) = {id_commu} RETURN a";
        $query_object = $this->entityManager->createQuery($query);
        $query_object->addEntityMapping("a", Avatar::class);
        if($community !== null)
            $query_object->setParameter("id_commu",$community->getId());
        return $query_object->execute();
    }
}
