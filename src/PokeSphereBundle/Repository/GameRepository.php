<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 4/27/2017
 * Time: 8:35 PM
 */
namespace PokeSphereBundle\Repository;
use GraphAware\Neo4j\OGM\Repository\BaseRepository;
use PokeSphereBundle\Entity\Game;
class GameRepository extends BaseRepository
{
    public function detachAndDeleteGame(Game $game){
        $em = $this->entityManager;
        $query = $em->createQuery("MATCH (g:Game) WHERE id(g) = {gameId} DETACH DELETE g");
        $query->setParameter('gameId', $game->getId());
        $query->execute();
    }
}
