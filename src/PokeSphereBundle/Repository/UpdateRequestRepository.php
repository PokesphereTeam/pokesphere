<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 4/13/2017
 * Time: 5:47 PM
 */
namespace PokeSphereBundle\Repository;
use GraphAware\Neo4j\OGM\Repository\BaseRepository;
use PokeSphereBundle\Entity\EnumRequestStatut;
use PokeSphereBundle\Entity\User;
abstract class UpdateRequestRepository extends BaseRepository
{
    public function setAllWaitingsToCanceled(User $user)
    {
        $em = $this->entityManager;
        $query = $em->createQuery('MATCH (r:'.$this->getLabelRequest().')<-[MAKE_REQUEST]-(u:User) 
                     WHERE id(u) = {idUser}
                        AND r.statut = {old_statut}
                      SET r.statut = {new_statut}'
        );
        $query->setParameter('idUser', $user->getId());
        $query->setParameter('old_statut', EnumRequestStatut::WAITING);
        $query->setParameter('new_statut', EnumRequestStatut::CANCELED);
        $query->execute();
    }
    public function getWaitingRequest(User $user, $token)
    {
        $em = $this->entityManager;
        $query = $em->createQuery('MATCH (r:'.$this->getLabelRequest().')<-[MAKE_REQUEST]-(u:User) 
                     WHERE id(u) = {idUser}
                        AND r.statut = {statut} AND r.token = {token}
                     RETURN r'
        );
        $query->addEntityMapping('r', $this->getClassRequest());
        $query->setParameter('requestLabel', $this->getLabelRequest());
        $query->setParameter('idUser', $user->getId());
        $query->setParameter('statut', EnumRequestStatut::WAITING);
        $query->setParameter('token', $token);
        $requests =  $query->execute();
        return empty($requests)?null:$requests[0];
    }
    public abstract function getClassRequest();
    public abstract function getLabelRequest();
}
