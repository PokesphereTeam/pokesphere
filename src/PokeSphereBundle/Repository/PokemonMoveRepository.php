<?php
namespace PokeSphereBundle\Repository;
use GraphAware\Neo4j\OGM\Repository\BaseRepository;
use PokeSphereBundle\Entity\GameVersion;
use PokeSphereBundle\Entity\Move;
use PokeSphereBundle\Entity\Pokemon;
use PokeSphereBundle\Entity\PokemonMove;
class PokemonMoveRepository extends BaseRepository
{
    public function findOneByPokemonAndMove(Pokemon $pokemon, Move $move) : ?PokemonMove
    {
        return $this->entityManager->createQuery("
            MATCH (p:Pokemon)-[p_pm:CAN_LEARN]-(pm:PokemonMove)-[pm_m:CAN_BE_LEARNED]-(m:Move)
            WHERE id(p)={pokemon_id} and id(m)={move_id}
            RETURN pm
        ")
            ->setParameter('pokemon_id', $pokemon->getId())
            ->setParameter('move_id',    $move->getId())
            ->addEntityMapping('pm', PokemonMove::class)
            ->getOneOrNullResult()[0];
    }
    public function findOneOrCreateByPokemonAndMove(Pokemon $pokemon, Move $move) : ?PokemonMove
    {
        $pokemonMove = $this->findOneByPokemonAndMove($pokemon, $move);
        if (! $pokemonMove)
        {
            $pokemonMove = $this->entityManager->createQuery("
                CREATE (pm:PokemonMove) RETURN pm
            ")
                ->addEntityMapping('pm', PokemonMove::class)
                ->getOneResult();
            $this->entityManager->createQuery("
                MATCH  (pm:PokemonMove), (p:Pokemon) 
                WHERE  id(pm)={pmId} AND id(p)={pId}
                CREATE (pm)-[r:CAN_LEARN]->(p)
            ")
                ->setParameter('pmId',  $pokemonMove->getId())
                ->setParameter('pId',   $pokemon->getId())
                ->execute();
            $this->entityManager->createQuery("
                MATCH  (pm:PokemonMove), (m:Move) 
                WHERE  id(pm)={pmId} AND id(m)={mId}
                CREATE (pm)-[r:CAN_BE_LEARNED]->(m)
            ")
                ->setParameter('pmId',  $pokemonMove->getId())
                ->setParameter('mId',   $move->getId())
                ->execute();
        }
        return $pokemonMove;
    }
    public function findOneByPokemonMoveVersion(Pokemon $pokemon, Move $move, GameVersion $version) : ?PokemonMove
    {
        $pokemonMove = $this->entityManager->createQuery("
            MATCH 
                (p:Pokemon)-[p_pm:CAN_LEARN]-(pm:PokemonMove)-[pm_m:CAN_BE_LEARNED]-(m:Move), 
                (pm)-[pm_v:CAN_BE_LEARNED_IN_VERSION]-(v:GameVersion)
            WHERE id(p)={pokemon_id} and id(m)={move_id} and id(v)={version_id}
            RETURN pm
        ")
            ->setParameter('pokemon_id', $pokemon->getId())
            ->setParameter('move_id',    $move->getId())
            ->setParameter('version_id', $version->getId())
            ->addEntityMapping('pm', PokemonMove::class)
            ->getOneOrNullResult()[0];
        return $pokemonMove;
    }
    public function findOrCreateByPokemonMoveVersion(Pokemon $pokemon, Move $move, GameVersion $version) : PokemonMove
    {
        $pokemonMove = $this->findOneByPokemonMoveVersion($pokemon, $move, $version);
        if (! $pokemonMove)
        {
            // on cherche un PokemonMove associant ce pokemon à cette attaque, ou on le crée
            // puis on le lie à la version
            $pokemonMove = $this->findOneOrCreateByPokemonAndMove($pokemon, $move);
            $this->entityManager->createQuery("
                MATCH  (pm:PokemonMove), (gv:GameVersion) 
                WHERE  id(pm)={pmId} AND id(gv)={gvId}
                CREATE (pm)-[r:CAN_BE_LEARNED_IN_VERSION]->(gv)
            ")
                ->setParameter('pmId',  $pokemonMove->getId())
                ->setParameter('gvId',  $version->getId())
                ->execute();
        }
        return $pokemonMove;
    }
}
