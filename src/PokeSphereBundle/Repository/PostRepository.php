<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 5/13/2017
 * Time: 7:11 PM
 */
namespace PokeSphereBundle\Repository;
use GraphAware\Neo4j\OGM\Repository\BaseRepository;
use PokeSphereBundle\Entity\Post;
class PostRepository extends BaseRepository
{
    public function clearAllShares(Post $post){
        $query = $this->entityManager->createQuery(
            "MATCH ()-[sh:SHARED]-(p:Post)
            WHERE id(p) = {post_id}
            DELETE sh"
        );
        $query->setParameter("post_id", $post->getId());
        $query->execute();
    }
    public function sharePost(int $id, int $idPost)
    {
        $query = $this->entityManager->createQuery(
            "MATCH (n),(p:Post)
            WHERE id(p) = {post_id} AND id(n) = {node_id}
            MERGE (n)-[:SHARED]->(p)"
        );
        $query->setParameter("post_id", $idPost);
        $query->setParameter("node_id", $id);
        $query->execute();
    }
    public function unsharePost(int $id, int $idPost)
    {
        $query = $this->entityManager->createQuery(
            "MATCH (n)-[s:SHARED]->(p:Post)
            WHERE id(p) = {post_id} AND id(n) = {node_id}
            DELETE s"
        );
        $query->setParameter("post_id", $idPost);
        $query->setParameter("node_id", $id);
        $query->execute();
    }
    public function hasSharedPost(int $id, int $idPost)
    {
        $query = $this->entityManager->createQuery(
            "MATCH (n)-[s:SHARED]->(p:Post)
            WHERE id(p) = {post_id} AND id(n) = {node_id}
            RETURN p"
        );
        $query->setParameter("post_id", $idPost);
        $query->setParameter("node_id", $id);
        $query->addEntityMapping("p",Post::class);
        return $query->execute();
    }
    public function getSharedPosts(int $id, ?int $skip = null, ?int $limit = null){
        $squery = "MATCH (n)-[:SHARED]->(p:Post)
                    WHERE id(n) = {node_id}
                    RETURN p ";
        if($skip)
            $squery.="SKIP {skip} ";
        if($limit)
            $squery.="LIMIT {limit} ";
        $query = $this->entityManager->createQuery($squery);
        $query->setParameter("node_id", $id);
        $query->addEntityMapping("p",Post::class);
        if($skip)
            $query->setParameter('skip',$skip);
        if($limit)
            $query->setParameter('limit',$limit);
        return $query->execute();
    }
    public function getPublicSharedPosts(int $id, ?int $skip = null, ?int $limit = null){
        $squery = "MATCH (n)-[:SHARED]->(p:PublicPost)
                    WHERE id(n) = {node_id}
                    RETURN p";
        if($skip)
            $squery.="SKIP {skip} ";
        if($limit)
            $squery.="LIMIT {limit} ";
        $query = $this->entityManager->createQuery($squery);
        $query->setParameter("node_id", $id);
        $query->addEntityMapping("p",Post::class);
        if($skip)
            $query->setParameter('skip',$skip);
        if($limit)
            $query->setParameter('limit',$limit);
        return $query->execute();
    }
}
