<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 4/14/2017
 * Time: 11:24 AM
 */
namespace PokeSphereBundle\Repository;
use PokeSphereBundle\Entity\EmailRequest;
class EmailRequestRepository extends UpdateRequestRepository
{
    public function getClassRequest()
    {
        return EmailRequest::class;
    }
    public function getLabelRequest()
    {
        return "EmailRequest";
    }
}
