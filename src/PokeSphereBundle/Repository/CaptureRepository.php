<?php
namespace PokeSphereBundle\Repository;
use GraphAware\Neo4j\OGM\Repository\BaseRepository;
use PokeSphereBundle\Entity\Capture;
class CaptureRepository extends BaseRepository
{
    public function findByUserId(int $userId, int $limit, int $skip)
    {
        return $this->entityManager->createQuery("
            MATCH (u:User)-[pw:PLAY_WITH]-(g:Game), (g)-[ci:CAUGHT_IN]-(c:Capture)
            WHERE id(u)={userId}
            RETURN c SKIP {skip} LIMIT {limit}
        ")
            ->setParameter('userId', $userId)
            ->setParameter('limit',  $limit)
            ->setParameter('skip',   $skip)
            ->addEntityMapping('c', Capture::class)
            ->execute();
    }
}
