<?php
namespace PokeSphereBundle\Repository;
use GraphAware\Neo4j\OGM\Repository\BaseRepository;
use PokeSphereBundle\Entity\GameVersion;
class GameVersionRepository extends BaseRepository
{
    public function findByPoketoolsIdOrCreate(int $poketoolsId) : GameVersion
    {
        /** @var GameVersion $version */
        $version = $this->findOneBy(['poketoolsId' => $poketoolsId]);
        if (! $version)
        {
            $version = $this->entityManager->createQuery(
                "CREATE (gv:GameVersion {poketoolsId: {poketoolsId}}) RETURN gv"
            )
                ->setParameter('poketoolsId', $poketoolsId)
                ->addEntityMapping('gv', GameVersion::class)
                ->getOneResult();
        }
        return $version;
    }
}
