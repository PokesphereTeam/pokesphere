<?php
namespace PokeSphereBundle\Repository;
use GraphAware\Neo4j\OGM\Repository\BaseRepository;
use PokeSphereBundle\Entity\Move;
class MoveRepository extends BaseRepository
{
    public function findOneByNameFrCI(string $name) : ?Move
    {
        return $this->entityManager
            ->createQuery("MATCH (m:Move) WHERE m.name_fr =~ {name_fr} RETURN m")
            ->addEntityMapping('m', Move::class)
            ->setParameter('name_fr', "(?ui)".self::prepareAccentInsensitiveRegex($name))
            ->getOneOrNullResult()[0];
    }
    public static function prepareAccentInsensitiveRegex(string $name)
    {
        $name = preg_replace('/(oe|œ|Œ)/i',    '(œ|Œ)',       $name);
        $name = preg_replace('/(é|è|ê|ë|e)/i', '(é|è|ê|ë|e)', $name);
        $name = preg_replace('/(\'| |-)/',     '(\'| |-)',    $name);
        return $name;
    }
    public function fixBulbapediaMoveNamesFr()
    {
        $this->entityManager->createQuery("MATCH (n:Move {bulbapediaId: 27})  set n.name_fr='Mawashi Geri'       return n")->execute();
        $this->entityManager->createQuery("MATCH (n:Move {bulbapediaId: 55})  set n.name_fr='Pistolet à O'       return n")->execute();
        $this->entityManager->createQuery("MATCH (n:Move {bulbapediaId: 61})  set n.name_fr='Bulles d\'O'        return n")->execute();
        $this->entityManager->createQuery("MATCH (n:Move {bulbapediaId: 176}) set n.name_fr='Conversion 2'       return n")->execute();
        $this->entityManager->createQuery("MATCH (n:Move {bulbapediaId: 194}) set n.name_fr='Prélèvement Destin' return n")->execute();
        $this->entityManager->createQuery("MATCH (n:Move {bulbapediaId: 309}) set n.name_fr='Poing Météore'      return n")->execute();
        $this->entityManager->createQuery("MATCH (n:Move {bulbapediaId: 312}) set n.name_fr='Aromathérapie'      return n")->execute();
        $this->entityManager->createQuery("MATCH (n:Move {bulbapediaId: 434}) set n.name_fr='Draco Météore'      return n")->execute();
        $this->entityManager->createQuery("MATCH (n:Move {bulbapediaId: 517}) set n.name_fr='Feu d\'Enfer'       return n")->execute();
        $this->entityManager->createQuery("MATCH (n:Move {bulbapediaId: 531}) set n.name_fr='Crèvecœur'          return n")->execute();
        $this->entityManager->createQuery("MATCH (n:Move {bulbapediaId: 534}) set n.name_fr='Coquilame'          return n")->execute();
        $this->entityManager->createQuery("MATCH (n:Move {bulbapediaId: 543}) set n.name_fr='Peignée'            return n")->execute();
    }
}
