<?php
/**
 * Created by PhpStorm.
 * User: Sobraz
 * Date: 20/05/2017
 * Time: 11:18
 */
namespace PokeSphereBundle\Repository;
use GraphAware\Neo4j\OGM\EntityManager;
use GraphAware\Neo4j\OGM\Query;
use GraphAware\Neo4j\OGM\Repository\BaseRepository;
use PokeSphereBundle\Entity\Community;
use PokeSphereBundle\Entity\Profile;
use PokeSphereBundle\Entity\User;
class ProfileRepository extends BaseRepository
{
    /*
    public function searchProfileFromUser($userid, $terms, int $limit, int $skip){
        if(!$terms)
            return null;
        $em = $this->entityManager;
        $squery = "
            MATCH(n), (u:User) 
            WHERE 	(n:User OR n:Community) 
                    AND ((exists(n.name) AND n.name =~ {terms_commu}) 
                        OR (exists(n.pseudo) AND n.pseudo =~ {terms_user}))
                    AND id(u) = {userid}
            WITH    n, 
                    CASE WHEN exists(n.name) THEN n.name ELSE n.pseudo END as name, 
                    exists((n)--(u)) as isconnected
            RETURN n
            ORDER BY isconnected DESC, name
        ";
        if($skip)
            $squery.="SKIP {skip} ";
        if($limit)
            $squery.="LIMIT {limit} ";
        $query = $em->createQuery($squery);
        $query->setParameter('terms_commu',"(?i)^".$terms."(.*)");
        $query->setParameter('terms_user',"(?i)^".$terms."(.*)");
        $query->setParameter('userid',$userid);
        if($skip)
            $query->setParameter('skip',$skip);
        if($limit)
            $query->setParameter('limit',$limit);
        $query->addEntityMapping('n',Profile::class);
        return $query->execute();
    }
    */
    public function searchProfileFromGuest($terms, int $limit, int $skip){
        if(!$terms)
            return null;
        /** @var EntityManager $em */
        $em = $this->entityManager;
        $squery = "
            MATCH (n)
            WHERE n:User OR n:Community
            WITH n, 
                CASE WHEN exists(n.name) THEN n.name ELSE n.pseudo END as name
            WHERE name =~ {terms}
            WITH n, name 
            ORDER BY name ";
        if($skip > 0)
            $squery.="SKIP {skip} ";
        if($limit > 0)
            $squery.="LIMIT {limit} ";
        $squery.=  "
            WITH 	filter(u in collect(n) WHERE 'User' IN labels(u)) as users, 
                    filter(m in collect(n) WHERE 'Community' IN labels(m)) as communities
            RETURN users, communities";
        $query = $em->createQuery($squery);
        $query->setParameter('terms',"(?i)^".$terms."(.*)");
        if($skip > 0)
            $query->setParameter('skip',$skip);
        if($limit > 0)
            $query->setParameter('limit',$limit);
        $query->addEntityMapping('users',User::class,Query::HYDRATE_COLLECTION);
        $query->addEntityMapping('communities',Community::class,Query::HYDRATE_COLLECTION);
        $result = $query->execute();
        return $result[0];
    }
}
