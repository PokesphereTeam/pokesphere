<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 4/13/2017
 * Time: 10:26 AM
 */
namespace PokeSphereBundle\Repository;
use PokeSphereBundle\Entity\ActivationRequest;
class ActivationRequestRepository extends UpdateRequestRepository
{
    public function getClassRequest()
    {
        return ActivationRequest::class;
    }
    public function getLabelRequest()
    {
        return "ActivationRequest";
    }
}
