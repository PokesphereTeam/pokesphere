<?php
namespace PokeSphereBundle\Repository;
use GraphAware\Neo4j\OGM\Repository\BaseRepository;
use PokeSphereBundle\Entity\Stat;
class StatRepository extends BaseRepository
{
    public function searchStat(string $pokemon) : array
    {
        $em = $this->entityManager;
        $query = $em->createQuery('MATCH (stat:Stat) 
                     WHERE stat.pokemon =~ {pokemon}
                     RETURN stat'
        );
        $query->addEntityMapping('stat', Stat::class);
        $query->setParameter('pokemon', $pokemon);
        return $query->execute();
    }
}

