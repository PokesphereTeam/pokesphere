<?php
namespace PokeSphereBundle\Repository;
use GraphAware\Neo4j\OGM\Repository\BaseRepository;
use PokeSphereBundle\Entity\Evolution;
use PokeSphereBundle\Entity\Pokemon;
use PokeSphereBundle\Entity\PokemonTalent;
use PokeSphereBundle\Entity\Talent;
class PokemonRepository extends BaseRepository
{
    public function findTalentRelation(Pokemon $pokemon, Talent $talent, bool $orCreate=false) : ?PokemonTalent
    {
        foreach ($pokemon->getPokemonTalents() as $pokemonTalent)
        {
            if ($pokemonTalent->getTalent() === $talent)
                return $pokemonTalent;
        }
        if ($orCreate)
        {
            $pokemonTalent = new PokemonTalent();
            $pokemonTalent->setPokemon($pokemon);
            $pokemonTalent->setTalent($talent);
            $pokemon->getPokemonTalents()->add($pokemonTalent);
            return $pokemonTalent;
        }
        return null;
    }
    public function findEvolutionRelation(Pokemon $pokemon, Pokemon $evolution, bool $orCreate=false) : ?Evolution
    {
        foreach ($pokemon->getEvolutions() as $pokemonEvolution)
        {
            if ($pokemonEvolution->getEvolution() === $evolution)
                return $pokemonEvolution;
        }
        if ($orCreate)
        {
            $pokemonEvolution = new Evolution();
            $pokemonEvolution->setAntecedant($pokemon);
            $pokemonEvolution->setEvolution($evolution);
            $pokemon->getEvolutions()->add($pokemonEvolution);
            return $pokemonEvolution;
        }
        return null;
    }
}
