<?php
namespace PokeSphereBundle\Repository;
use GraphAware\Neo4j\OGM\Repository\BaseRepository;
use PokeSphereBundle\Entity\Session;
class SessionRepository extends BaseRepository
{
    /**
     * @return Session[]
     */
    public function findExpiredSessions() : array
    {
        $query = $this->entityManager->createQuery('
            MATCH (s:Session) 
            WHERE s.sess_lifetime + sess_time < {time}
            RETURN s'
        );
        $query->addEntityMapping('s', Session::class);
        $query->setParameter('time', time());
        return $query->execute();
    }
}

