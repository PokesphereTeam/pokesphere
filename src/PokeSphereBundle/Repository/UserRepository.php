<?php
namespace PokeSphereBundle\Repository;
use GraphAware\Neo4j\OGM\Repository\BaseRepository;
use PokeSphereBundle\Entity\User;
class UserRepository extends BaseRepository
{
    public function pseudoCaseInsensitiveExist(string $terms) : array
    {
        $em = $this->entityManager;
        $query = $em->createQuery('MATCH (u:User) 
                     WHERE u.pseudo =~ {pattern}
                     RETURN u'
        );
        $query->addEntityMapping('u', User::class);
        $query->setParameter('pattern', "(?i)".$terms);
        return $query->execute();
    }
}
