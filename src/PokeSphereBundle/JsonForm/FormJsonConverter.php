<?php
/**
 * Created by PhpStorm.
 * User: robin
 * Date: 18/04/2017
 * Time: 11:17
 */
namespace PokeSphereBundle\JsonForm;
use Symfony\Component\Form\Form;
class FormJsonConverter
{
    public final function formErrorsToArray(Form $form) : array
    {
        $errors = [];
        foreach ($form->getErrors() as $error) {
            $errors[$form->getName()][] = $error->getMessage();
        }
        // Fields
        foreach ($form as $child /** @var Form $child */) {
            if (!$child->isValid()) {
                foreach ($child->getErrors(true) as $error) {
                    $errors[$child->getName()][] = $error->getMessage();
                }
            }
        }
        return $errors;
    }
}
