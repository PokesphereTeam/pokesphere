<?php
/**
 * Created by PhpStorm.
 * User: robin
 * Date: 22/04/2017
 * Time: 13:55
 */
namespace PokeSphereBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
class LogoutSuccessHandler implements LogoutSuccessHandlerInterface
{
    /**
     * Creates a Response object to send upon a successful logout.
     *
     * @param Request $request
     *
     * @return Response never null
     */
    public function onLogoutSuccess(Request $request)
    {
        $response = new JsonResponse([], 204);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
