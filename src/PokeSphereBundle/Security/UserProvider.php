<?php
namespace PokeSphereBundle\Security;
use GraphAware\Neo4j\OGM\EntityManager;
use GraphAware\Neo4j\OGM\Events;
use PokeSphereBundle\Entity\SecurityUser;
use PokeSphereBundle\Entity\User;
use PokeSphereBundle\Listener\PrePersistUser;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface as SecurityUserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
class UserProvider implements UserProviderInterface, AuthenticationEntryPointInterface
{
    /**
     * @var EntityManager
     */
    protected $manager;
    /**
     * Constructor.
     *
     * @param EntityManager $neo4j_manager
     */
    public function __construct(EntityManager $neo4j_manager)
    {
        $this->manager = $neo4j_manager;
    }
    #region AuthEntryPoint
    /**
     * {@inheritdoc}
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $response = new Response('', 401);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    #endregion
    #region UserProvider
    /**
     * {@inheritDoc}
     */
    public function loadUserByUsername($username)
    {
        /** @var User $user */
        $repo = $this->manager->getRepository(User::class);
        $user = $repo->findOneBy(['email' => $username]);
        if (!$user) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }
        $suser = new SecurityUser($user);
        return $suser;
    }
    /**
     * {@inheritDoc}
     */
    public function refreshUser(SecurityUserInterface $user)
    {
        /** @var User $reloadedUser */
        if (null === $reloadedUser = $this->manager->getRepository(User::class)->findOneById($user->getId())) {
            throw new UsernameNotFoundException(sprintf('User with ID "%d" could not be reloaded.', $user->getId()));
        }
        return new SecurityUser($reloadedUser);
    }
    /**
     * {@inheritDoc}
     */
    public function supportsClass($class)
    {
        $userClass = $this->manager->getClass();
        return $userClass === $class || is_subclass_of($class, $userClass);
    }
    #endregion
}

