<?php
namespace PokeSphereBundle\Listener;
use GraphAware\Neo4j\OGM\EntityManager;
use PokeSphereBundle\Entity\Capture;
use PokeSphereBundle\Entity\PokemonMove;
use PokeSphereBundle\Repository\PokemonMoveRepository;
use PokeSphereBundle\Repository\PokemonRepository;
class PrePersistCapture
{
    const EV_MAX       = 252;
    const EV_TOTAL_MAX = 510;
    /** @var Capture $entity */
    private $entity;
    private $neo4j;
    public function __construct(EntityManager $neo4j)
    {
        $this->neo4j = $neo4j;
    }
    public function onPreFlushSave(EntityEvent $entityEvent)
    {
        $this->entity = $entityEvent->getEntity();
        $this->checkEv();
        $this->checkMoves();
    }
    protected function checkEv()
    {
        $e = $this->entity;
        if (
            $e->getHpEv()     > self::EV_MAX || $e->getSpeedEv()  > self::EV_MAX ||
            $e->getAtkEv()    > self::EV_MAX || $e->getDefEv()    > self::EV_MAX ||
            $e->getSpeAtkEv() > self::EV_MAX || $e->getSpeDefEv() > self::EV_MAX
        )
            throw new \Exception("An EV cannot exceed ".self::EV_MAX);
        
        if ($e->getHpEv() + $e->getAtkEv() + $e->getDefEv() + $e->getSpeAtkEv() + $e->getSpeDefEv() + $e->getSpeedEv() > self::EV_TOTAL_MAX)
            throw new \Exception("The sum of the EVs cannot exceed ".self::EV_TOTAL_MAX);
    }
    protected function checkMoves()
    {
        $moves = $this->entity->getMoves();
        if ($moves->count() < 1 || $moves->count() > 4)
            throw new \Exception("A pokemon must have [1, 4] moves");
        /** @var PokemonMoveRepository $pokemonMoveRepo */
        $pokemonMoveRepo = $this->neo4j->getRepository(PokemonMove::class);
        foreach ($moves as $move)
        {
            $pokemon = $this->entity->getPokemon();
            $version = $this->entity->getGame()->getGameVersion();
            $pkMove = $pokemonMoveRepo->findOneByPokemonMoveVersion($pokemon, $move, $version);
            if (! $pkMove)
                throw new \Exception("The move #{$move->getId()} cannot be learnt by pokemon {$pokemon->getId()} in version {$version->getId()}");
        }
    }
}
