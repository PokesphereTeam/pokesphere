<?php
namespace PokeSphereBundle\Listener;
use Doctrine\Common\EventArgs;
use GraphAware\Neo4j\OGM\Event\OnFlushEventArgs;
use GraphAware\Neo4j\OGM\Event\PostFlushEventArgs;
use GraphAware\Neo4j\OGM\Event\PreFlushEventArgs;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
abstract class AbstractNeo4jListener
{
    private $eventDispatcher;
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }
    /**
     * @param PreFlushEventArgs|OnFlushEventArgs|PostFlushEventArgs $eventArgs
     * @param string $eventName
     */
    public function onEvent(EventArgs $eventArgs, string $eventName)
    {
        foreach ($eventArgs->getEntityManager()->getUnitOfWork()->getNodesScheduledForCreate() as $entity)
        {
            $this->doOnEvent($entity, $eventName . '_CREATE');
            $this->doOnEvent($entity, $eventName . '_SAVE');
        }
        foreach ($eventArgs->getEntityManager()->getUnitOfWork()->getNodesScheduledForUpdate() as $entity)
        {
            $this->doOnEvent($entity, $eventName . '_UPDATE');
            $this->doOnEvent($entity, $eventName . '_SAVE');
        }
        foreach ($eventArgs->getEntityManager()->getUnitOfWork()->getNodesScheduledForDelete() as $entity)
        {
            $this->doOnEvent($entity, $eventName . '_DELETE');
        }
    }
    public function doOnEvent($entity, $eventName)
    {
        // extract the class name from the FQCN
        $splitClassName = explode('\\', get_class($entity));
        $classname = strtoupper(end($splitClassName));
        try
        {
            $constant = constant('PokeSphereBundle\Listener\EntityEvent::'.$eventName.'_'.$classname);
            $this->eventDispatcher->dispatch($constant, new EntityEvent($entity));
        }
        catch (\Exception $e)
        {
            // no listener for this event on this entity
        }
    }
}
