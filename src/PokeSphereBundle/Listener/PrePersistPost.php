<?php
namespace PokeSphereBundle\Listener;
use PokeSphereBundle\Entity\Post;
class PrePersistPost
{
    public function onPreFlushSave(EntityEvent $entityEvent)
    {
        /** @var Post $entity */
        $entity = $entityEvent->getEntity();
        $dt = new \DateTime("NOW", new \DateTimeZone("UTC"));
        if($entity->getCreatedAt() == null)
            $entity->setCreatedAt($dt);
    }
}
