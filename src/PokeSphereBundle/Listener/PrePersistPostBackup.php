<?php
namespace PokeSphereBundle\Listener;
use PokeSphereBundle\Entity\PostBackup;
class PrePersistPostBackup
{
    public function onPreFlushSave(EntityEvent $entityEvent)
    {
        /** @var PostBackup $entity */
        $entity = $entityEvent->getEntity();
        $dt = new \DateTime("NOW", new \DateTimeZone("UTC"));
        if($entity->getCreatedAt() === null)
            $entity->setCreatedAt($dt);
    }
}
