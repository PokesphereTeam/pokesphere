<?php
namespace PokeSphereBundle\Listener;
use Symfony\Component\EventDispatcher\Event;
final class EntityEvent extends Event
{
    /**
     * Strict naming must be respected unless the listener won't see the event
     * NEO4JEVENT _ ACTION _ ENTITYCLASS
     *
     * NEO4JEVENT : @see \GraphAware\Neo4j\OGM\Events
     * ACTION : CREATE, UPDATE, SAVE (=CREATE or UPDATE), DELETE
     * ENTITYCLASS : the class name of the entity without namespace
     */
    const PREFLUSH_SAVE_POST       = 'pokesphere.pre_flush_save_post';
    const PREFLUSH_SAVE_POSTBACKUP = 'pokesphere.pre_flush_save_postbackup';
    const PREFLUSH_SAVE_REQUEST    = 'pokesphere.pre_flush_save_request';
    const PREFLUSH_SAVE_USER       = 'pokesphere.pre_flush_save_user';
    const PREFLUSH_SAVE_CAPTURE    = 'pokesphere.pre_flush_save_capture';
    const ONFLUSH_SAVE_CAPTURE     = 'pokesphere.on_flush_save_capture';
    const POSTFLUSH_SAVE_CAPTURE   = 'pokesphere.post_flush_save_capture';
    private $entity;
    public function __construct($entity)
    {
        $this->entity = $entity;
    }
    public function getEntity()
    {
        return $this->entity;
    }
    public function setEntity($entity)
    {
        $this->entity = $entity;
        return $this;
    }
}
