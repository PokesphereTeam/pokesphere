<?php
namespace PokeSphereBundle\Listener;
use PokeSphereBundle\Entity\UpdateRequest;
class PrePersistRequest
{
    public function onPreFlushSave(EntityEvent $entityEvent)
    {
        /** @var UpdateRequest $entity */
        $entity = $entityEvent->getEntity();
        $dt = new \DateTime("NOW", new \DateTimeZone("UTC"));
        if($entity->getCreatedAt() === null)
            $entity->setCreatedAt($dt->getTimestamp());
    }
}
