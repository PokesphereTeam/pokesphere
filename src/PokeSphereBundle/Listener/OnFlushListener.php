<?php
namespace PokeSphereBundle\Listener;
use GraphAware\Neo4j\OGM\Event\OnFlushEventArgs;
class OnFlushListener extends AbstractNeo4jListener
{
    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        parent::onEvent($eventArgs, 'ONFLUSH');
    }
}
