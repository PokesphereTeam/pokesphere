<?php
namespace PokeSphereBundle\Listener;
use GraphAware\Neo4j\OGM\EntityManager;
use GraphAware\Neo4j\OGM\Events;
use Psr\Container\ContainerInterface;
class InitNeo4jEventsListener
{
    private $container;
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    public function initialize()
    {
        $neo4j = $this->container->get('neo4j');
        $neo4j->getEventManager()->addEventListener(Events::PRE_FLUSH,  $this->container->get('pokesphere.preflush_listener'));
        $neo4j->getEventManager()->addEventListener(Events::ON_FLUSH,   $this->container->get('pokesphere.onflush_listener'));
        $neo4j->getEventManager()->addEventListener(Events::POST_FLUSH, $this->container->get('pokesphere.postflush_listener'));
    }
}
