<?php
namespace PokeSphereBundle\Listener;
use GraphAware\Neo4j\OGM\Event\PostFlushEventArgs;
class PostFlushListener extends AbstractNeo4jListener
{
    public function postFlush(PostFlushEventArgs $eventArgs)
    {
        parent::onEvent($eventArgs, 'POSTFLUSH');
    }
}
