<?php
namespace PokeSphereBundle\Listener;
use GraphAware\Neo4j\OGM\Event\PreFlushEventArgs;
class PreFlushListener extends AbstractNeo4jListener
{
    public function preFlush(PreFlushEventArgs $eventArgs)
    {
        parent::onEvent($eventArgs, 'PREFLUSH');
    }
}
