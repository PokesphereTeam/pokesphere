<?php
namespace PokeSphereBundle\Listener;
use PokeSphereBundle\Entity\User;
class PrePersistUser
{
    public function onPreFlushSave(EntityEvent $entityEvent)
    {
        /** @var User $entity */
        $entity = $entityEvent->getEntity();
        $dt = new \DateTime("NOW", new \DateTimeZone("UTC"));
        if($entity->getCreatedAt() === null)
            $entity->setCreatedAt($dt);
    }
}
