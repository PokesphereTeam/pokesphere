<?php
namespace PokeSphereBundle\Entity;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @OGM\Node(label="User", repository="PokeSphereBundle\Repository\UserRepository")
 */
class User
{
    public function __construct()
    {
        $this->appRoles = new ArrayCollection();
    }
    #region Attributes
    /**
     * @var int
     * @OGM\GraphId()
     * @Groups({"userPublicInfo","details"})
     */
    private $id;
    /**
     * @OGM\Property(type="string")
     * @var string
     * @Groups({"userPublicInfo","details"})
     * @Assert\NotNull(message="user.pseudo.required", groups={"registration"})
     * @Assert\Length(min="4", max="16", minMessage="user.pseudo.length", maxMessage="user.pseudo.length", groups={"registration"})
     * @Assert\Regex(pattern="/^([a-zA-Z]|\d)*$/i", message="user.pseudo.pattern", groups={"registration"})
     */
    private $pseudo;
    /**
     * @OGM\Property(type="string")
     * @var string
     * @Groups({"details"})
     * @Assert\NotNull(message="user.email.required", groups={"registration"})
     * @Assert\Email(groups={"registration","changeEmail","details"})
     */
    private $email;
    /**
     * @OGM\Property(type="string")
     * @var string
     * @Assert\NotNull(message="user.password.required", groups={"registration"})
     * @Assert\Length(min=8, max=15, minMessage="user.password.length", maxMessage="user.password.length", groups={"registration","changePassword"})
     * @Assert\Regex(pattern="/^([a-z]|\d)*[A-Z]([a-zA-Z]|\d)*$/", message="user.password.contain.capital", groups={"registration","changePassword"})
     * @Assert\Regex(pattern="/^([A-Z]|\d)*[a-z]([a-zA-Z]|\d)*$/", message="user.password.contain.letter", groups={"registration","changePassword"})
     * @Assert\Regex(pattern="/^([a-zA-Z]|)*\d([a-zA-Z]|\d)*$/", message="user.password.contain.number", groups={"registration","changePassword"})
     */
    private $password;
    /**
     * @OGM\Property(type="boolean")
     * @var bool
     */
    private $isValidated = false;
    /**
     * @var DateTime
     * @OGM\Property()
     * @OGM\Convert(type="datetime", options={"format":"timestamp"})
     * @Groups({"userPublicInfo"})
     */
    private $createdAt;
    /**
     * @OGM\Property(type="boolean")
     * @var bool
     */
    private $isActive = true;
    /**
     * @OGM\Relationship(type="IS_ROLE", direction="OUTGOING", targetEntity="Role", collection=true)
     * @var ArrayCollection | Role[]
     */
    private $appRoles = [];
    /**
     * @var DateTime
     * @OGM\Property()
     * @OGM\Convert(type="datetime", options={"format":"timestamp"})
     * @Groups({"userPrivateInfo","details"})
     * @Assert\DateTime(format="d-m-Y", message="user.dateofbirth.pattern", groups={"registration","details"})
     * @Assert\LessThan("-13 years", message="user.dateofbirth.tooyoung", groups={"registration","details"})
     * @Assert\GreaterThan("-80 years", message="user.dateofbirth.tooold", groups={"registration","details"})
     */
    private $dateOfBirth;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Groups({"userPrivateInfo","details"})
     * @Assert\Regex(pattern="/^\d{12}$/", message="user.ca.pattern", groups={"registration","details"})
     */
    private $friendCode;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Groups({"details","userPublicInfo"})
     * @Assert\Length(max="200", maxMessage="user.description.maxlength", groups={"details"})
     */
    private $description;
    /**
     * @var boolean
     * @OGM\Property(type="boolean")
     * @Groups({"details"})
     */
    private $isAMan;
    /**
     * @OGM\Relationship(type="CHOOSE_AVATAR", targetEntity="Avatar", direction="OUTGOING", collection=false)
     * @Groups({"details","userPublicInfo"})
     * @Assert\NotNull(message="user.avatar.notnull", groups={"details"})
     */
    private $avatar;
    /**
     * @OGM\Relationship(type="IS_SPHEREZONED", direction="BOTH", mappedBy="sphereZone", targetEntity="User", collection=true)
     * @var ArrayCollection | User[]
     */
    private $sphereZone = [];
    /**
     * @OGM\Relationship(type="REQUESTED_FOR_SPHEREZONE", direction="INCOMING", mappedBy="sphereZoneRequested", targetEntity="User", collection=true)
     * @var ArrayCollection | User[]
     */
    private $sphereZoneRequesters = [];
    /**
     * @OGM\Relationship(type="REQUESTED_FOR_SPHEREZONE", direction="OUTGOING", mappedBy="sphereZoneRequesters", targetEntity="User", collection=true)
     * @var ArrayCollection | User[]
     */
    private $sphereZoneRequested = [];
    /**
     * @OGM\Relationship(type="PLAY_WITH", direction="OUTGOING", mappedBy="user", targetEntity="Game", collection=true)
     * @var ArrayCollection | Game[]
     */
    private $games = [];
    /**
     * @var Community[]
     * @OGM\Relationship(type="MANAGED_BY", direction="INCOMING", targetEntity="Community", collection=true, mappedBy="followers")
     */
    private $managedCommunities;
    /**
     * @var Community[]
     * @OGM\Relationship(type="FOLLOW", direction="OUTGOING", targetEntity="Community", collection=true, mappedBy="followers")
     */
    private $followedCommunities;
    /**
     * @var Stat[]
     * @OGM\Relationship(type="APPROVED", direction="OUTGOING", targetEntity="Stat", collection=true, mappedBy="approvers")
     */
    private $approvedStats = [];
    #endregion
    #region Accessors
    public function getId()
    {
        return $this->id;
    }
    public function setId($id) : void
    {
        $this->id = $id;
    }
    public function setPseudo($pseudo) : User
    {
        $this->pseudo = $pseudo;
        return $this;
    }
    public function getPseudo()
    {
        return $this->pseudo;
    }
    public function setEmail($email) : User
    {
        $this->email = $email;
        return $this;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function setPassword($password) : User
    {
        $this->password = $password;
        return $this;
    }
    public function getPassword()
    {
        return $this->password;
    }
    public function setIsActive($isActive) : User
    {
        $this->isActive = $isActive?true:false;
        return $this;
    }
    public function getIsActive() : int
    {
        return $this->isActive;
    }
    public function getIsValidated() : int
    {
        return $this->isValidated;
    }
    public function setIsValidated(int $isValidated) : User
    {
        $this->isValidated = $isValidated?true:false;
        return $this;
    }
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    public function setCreatedAt($createdAt) : User
    {
        $this->createdAt = $createdAt;
        return $this;
    }
    public function setAppRoles($roles) : User
    {
        $this->appRoles = $roles;
        return $this;
    }
    public function getAppRoles()
    {
        return $this->appRoles;
    }
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
    }
    public function getFriendCode()
    {
        return $this->friendCode;
    }
    public function setFriendCode($friendCode)
    {
        $this->friendCode = $friendCode;
    }
    /**
     * @return ArrayCollection|User[]
     */
    public function getSphereZone()
    {
        return $this->sphereZone;
    }
    /**
     * @param ArrayCollection|User[] $sphereZone
     */
    public function setSphereZone($sphereZone)
    {
        $this->sphereZone = $sphereZone;
    }
    public function getSphereZonedUser(int $userid) : ?User
    {
        foreach ($this->getSphereZone() as $user)
        {
            if($user->getId() == $userid)
                return $user;
        }
        return null;
    }
    /**
     * @return ArrayCollection|User[]
     */
    public function getSphereZoneRequesters()
    {
        return $this->sphereZoneRequesters;
    }
    /**
     * @param ArrayCollection|User[] $sphereZoneRequesters
     */
    public function setSphereZoneRequesters($sphereZoneRequesters)
    {
        $this->sphereZoneRequesters = $sphereZoneRequesters;
    }
    public function getSphereZoneRequester(int $userid) : ?User
    {
        foreach ($this->getSphereZoneRequesters() as $user)
            if($user->getId() == $userid)
                return $user;
        return null;
    }
    /**
     * @return ArrayCollection|User[]
     */
    public function getSphereZoneRequested()
    {
        return $this->sphereZoneRequested;
    }
    /**
     * @param ArrayCollection|User[] $sphereZoneRequested
     */
    public function setSphereZoneRequested($sphereZoneRequested)
    {
        $this->sphereZoneRequested = $sphereZoneRequested;
    }
    public function getSphereZoneUserRequested(int $userid) : ?User
    {
        foreach ($this->getSphereZoneRequested() as $user)
        {
            if($user->getId() == $userid)
                return $user;
        }
        return null;
    }
    /**
     * @return ArrayCollection|Game[]
     */
    public function getGames()
    {
        return $this->games;
    }
    /**
     * @param ArrayCollection|Game[] $games
     */
    public function setGames($games)
    {
        $this->games = $games;
    }
    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }
    /**
     * @param mixed $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }
    /**
     * @return Community[]
     */
    public function getManagedCommunities()
    {
        return $this->managedCommunities;
    }
    /**
     * @param Community[] $managedCommunities
     */
    public function setManagedCommunities($managedCommunities)
    {
        $this->managedCommunities = $managedCommunities;
    }
    /**
     * @return Community[]
     */
    public function getFollowedCommunities()
    {
        return $this->followedCommunities;
    }
    /**
     * @param Community[] $followedCommunities
     */
    public function setFollowedCommunities($followedCommunities)
    {
        $this->followedCommunities = $followedCommunities;
    }
    public function hasFollow($comid)
    {
        foreach ($this->getFollowedCommunities() as $com)
        {
            if($com->getId() == $comid)
                return $com;
        }
        return null;
    }
    public function hasFollowByName($cname)
    {
        foreach ($this->getFollowedCommunities() as $com)
        {
            if($com->getName() === $cname)
                return $com;
        }
        return null;
    }
    public function isAMan(): ?bool
    {
        return $this->isAMan;
    }
    public function setIsAMan(?bool $isAMan)
    {
        $this->isAMan = $isAMan;
    }
    /**
     * @return Stat[]
     */
    public function getApprovedStats()
    {
        return $this->approvedStats;
    }
    /**
     * @param Stat[] $approvedStats
     */
    public function setApprovedStats($approvedStats)
    {
        $this->approvedStats = $approvedStats;
    }
    #endregion
}

