<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 4/22/2017
 * Time: 9:25 PM
 */
namespace PokeSphereBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use Symfony\Component\Serializer\Annotation\Groups;
/*
 * CREATE CONSTRAINT ON (entity:GameVersion) ASSERT entity.poketoolsId IS UNIQUE;
 */
/**
 * @OGM\Node(label="GameVersion", repository="PokeSphereBundle\Repository\GameVersionRepository")
 */
class GameVersion
{
    public function __construct()
    {
        $this->pokemonMoves = new ArrayCollection();
    }
    #region attributes
    /**
     * @var int
     * @OGM\GraphId()
     * @Groups({"versionInfo"})
     */
    private $id;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Groups({"versionInfo"})
     */
    private $name_fr;
    
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Groups({"versionInfo"})
     */
    private $name_en;
    
    /**
     * @var int
     * @OGM\Property(type="int")
     */
    private $poketoolsId;
    /**
     * @var Collection | PokemonMove[]
     * @OGM\Relationship(type="CAN_BE_LEARNED_IN_VERSION", direction="BOTH", collection=true, mappedBy="versions", targetEntity="PokemonMove")
     */
    private $pokemonMoves;
    #endregion
    #region accessors
    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getNameFr(): ?string
    {
        return $this->name_fr;
    }
    /**
     * @param string $name_fr
     */
    public function setNameFr(string $name_fr)
    {
        $this->name_fr = $name_fr;
    }
    /**
     * @return string
     */
    public function getNameEn(): ?string
    {
        return $this->name_en;
    }
    /**
     * @param string $name_en
     */
    public function setNameEn(string $name_en)
    {
        $this->name_en = $name_en;
    }
    public function getPoketoolsId() : int
    {
        return $this->poketoolsId;
    }
    public function setPoketoolsId(int $poketoolsId) : GameVersion
    {
        $this->poketoolsId = $poketoolsId;
        return $this;
    }
    public function getPokemonMoves()
    {
        return $this->pokemonMoves;
    }
    public function setPokemonMoves($pokemonMoves)
    {
        $this->pokemonMoves = $pokemonMoves;
        return $this;
    }
    #endregion
}
