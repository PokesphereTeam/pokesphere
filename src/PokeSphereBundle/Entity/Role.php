<?php
namespace PokeSphereBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use Symfony\Component\Security\Core\Role\RoleInterface;
/**
 * @OGM\Node(label="Role")
 */
class Role implements RoleInterface
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }
    #region properties
    /**
     * @OGM\GraphId()
     * @var int
     */
    private $id;
    /**
     * @OGM\Property(type="string")
     * @var string
     */
    private $role;
    /**
     * @OGM\Property(type="string")
     * @var string
     */
    private $libelle;
    /**
     * @OGM\Relationship(type="IS_ROLE", direction="INCOMING", targetEntity="User", collection=true)
     * @var ArrayCollection|User[]
     */
    private $users;
    #endregion
    /**
     * Get idRole
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set role
     *
     * @param string $role
     *
     * @return Role
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }
    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }
    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Role
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
        return $this;
    }
    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
    /**
     * Add user
     *
     * @param \PokeSphereBundle\Entity\User $user
     *
     * @return Role
     */
    public function addUser(\PokeSphereBundle\Entity\User $user)
    {
        $this->users[] = $user;
        return $this;
    }
    /**
     * Remove user
     *
     * @param \PokeSphereBundle\Entity\User $user
     */
    public function removeUser(\PokeSphereBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }
    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }
}

