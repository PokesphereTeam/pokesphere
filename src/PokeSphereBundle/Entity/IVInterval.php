<?php
namespace PokeSphereBundle\Entity;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * @OGM\Node(label="IVInterval")
 */
class IVInterval
{
    #region properties
    /**
     * @OGM\GraphId()
     * @Groups({"capture_info"})
     * @var int
     */
    private $id;
    /**
     * @OGM\Property(type="int")
     * @Groups({"capture_info"})
     * @var int
     */
    private $min;
    /**
     * @OGM\Property(type="int")
     * @Groups({"capture_info"})
     * @var int
     */
    private $max;
    #endregion
    #region
    public function getId() : int
    {
        return $this->id;
    }
    public function getMin() : int
    {
        return $this->min;
    }
    public function setMin(int $min) : IVInterval
    {
        $this->min = $min;
        return $this;
    }
    public function getMax() : int
    {
        return $this->max;
    }
    public function setMax(int $max) : IVInterval
    {
        $this->max = $max;
        return $this;
    }
    #enregion
}
