<?php
namespace PokeSphereBundle\Entity;
use Doctrine\Common\Collections\Collection as ICollection;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use Symfony\Component\Serializer\Annotation\Groups;
/*
 * load csv WITH HEADERS from "file:///Ball.csv" as line create (tmp:Ball {name_fr: line.nom_fr, name_en: line.nom_en, path: line.path})
 */
/**
 * @OGM\Node(label="Ball")
 */
class Ball
{
    #region properties
    /**
     * @OGM\GraphId()
     * @Groups({"capture_info"})
     * @var int
     */
    private $id;
    /**
     * @OGM\Property(type="string")
     * @var string
     */
    private $name_fr;
    /**
     * @OGM\Property(type="string")
     * @var string
     */
    private $name_en;
    /**
     * @OGM\Property(type="string")
     * @var string
     */
    private $path;
    /**
     * @OGM\Relationship(type="CAUGHT_WITH", direction="BOTH", targetEntity="Capture", collection=true, mappedBy="ball")
     * @var ICollection|Capture[]
     */
    private $captures;
    #endregion
    #region accessors
    public function getId() : int
    {
        return $this->id;
    }
    public function getNameFr() : string
    {
        return $this->name_fr;
    }
    public function setNameFr(string $name_fr) : Ball
    {
        $this->name_fr = $name_fr;
        return $this;
    }
    public function getNameEn() : string
    {
        return $this->name_en;
    }
    public function setNameEn(string $name_en) : Ball
    {
        $this->name_en = $name_en;
        return $this;
    }
    public function getPath() : string
    {
        return $this->path;
    }
    public function setPath(string $path) : Ball
    {
        $this->path = $path;
        return $this;
    }
    public function getCaptures() : ICollection
    {
        return $this->captures;
    }
    public function setCaptures(ICollection $captures) : Ball
    {
        $this->captures = $captures;
        return $this;
    }
    #endregion
}
