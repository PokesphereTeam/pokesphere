<?php
namespace PokeSphereBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use Symfony\Component\Serializer\Annotation\Groups;
/*
 * CREATE CONSTRAINT ON (entity:Move) ASSERT entity.bulbapediaId IS UNIQUE;
 */
/**
 * @OGM\Node(label="Move", repository="PokeSphereBundle\Repository\MoveRepository")
 */
class Move
{
    public function __construct()
    {
        $this->pokemonMoves = new ArrayCollection();
    }
    #region attributes
    /**
     * @OGM\GraphId()
     * @Groups({"capture_info", "pokemon_info"})
     * @var int
     */
    private $id;
    /**
     * @var int
     * @OGM\Property(type="int")
     */
    private $bulbapediaId;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Groups({"pokemon_info"})
     */
    private $name;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Groups({"pokemon_info"})
     */
    private $name_fr;
    /**
     * @var int
     * @OGM\Property(type="int")
     */
    private $pp;
    /**
     * @var int
     * @OGM\Property(type="int")
     */
    private $accuracy;
    /**
     * @var int
     * @OGM\Property(type="int")
     */
    private $generation;
    /**
     * @var int
     * @OGM\Property(type="int")
     */
    private $power;
//    /**
//     * @var Type
//     * @OGM\Relationship(type="IS_OF_TYPE", direction="OUTGOING", targetEntity="Type", collection=false, mappedBy="moves")
//     */
//    private $type;
//
//    /**
//     * @var MoveCategory
//     * @OGM\Relationship(type="IS_OF_MOVE_CATEGORY", direction="OUTGOING", targetEntity="MoveCategory", collection=false, mappedBy="moves")
//     */
//    private $moveCategory;
    /**
     * @var Collection
     * @OGM\Relationship(type="CAN_BE_LEARNED", direction="BOTH", targetEntity="PokemonMove", collection=true, mappedBy="move")
     */
    private $pokemonMoves;
    #endregion
    #region accessors
    public function getId() : int
    {
        return $this->id;
    }
    public function getBulbapediaId() : int
    {
        return $this->bulbapediaId;
    }
    public function setBulbapediaId(int $bulbapediaId) : Move
    {
        $this->bulbapediaId = $bulbapediaId;
        return $this;
    }
    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    public function getType()
    {
        return $this->type;
    }
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    public function getNameFr() : string
    {
        return $this->name_fr;
    }
    public function setNameFr(string $name_fr) : Move
    {
        $this->name_fr = $name_fr;
        return $this;
    }
    public function getPp() : int
    {
        return $this->pp;
    }
    public function setPp(int $pp) : Move
    {
        $this->pp = $pp;
        return $this;
    }
    public function getAccuracy() : int
    {
        return $this->accuracy;
    }
    public function setAccuracy(int $accuracy) : Move
    {
        $this->accuracy = $accuracy;
        return $this;
    }
    public function getGeneration() : int
    {
        return $this->generation;
    }
    public function setGeneration(int $generation) : Move
    {
        $this->generation = $generation;
        return $this;
    }
    public function getPower() : int
    {
        return $this->power;
    }
    public function setPower(int $power) : Move
    {
        $this->power = $power;
        return $this;
    }
    public function getMoveCategory() : MoveCategory
    {
        return $this->moveCategory;
    }
    public function setMoveCategory(MoveCategory $moveCategory) : Move
    {
        $this->moveCategory = $moveCategory;
        return $this;
    }
    public function getPokemonMoves() : Collection
    {
        return $this->pokemonMoves;
    }
    public function setPokemonMoves(Collection $pokemonMoves) : Move
    {
        $this->pokemonMoves = $pokemonMoves;
        return $this;
    }
    #endregion
}
