<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 4/22/2017
 * Time: 9:23 PM
 */
namespace PokeSphereBundle\Entity;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * @OGM\Node(label="GameSerie")
 */
class GameSerie
{
    /**
     * @var int
     * @OGM\GraphId()
     * @Groups({"serieInfo"})
     */
    private $id;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Groups({"serieInfo"})
     */
    private $acronym_en;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Groups({"serieInfo"})
     */
    private $acronym_fr;
    /**
     * @var Generation
     * @OGM\Relationship(type="BASED_ON", direction="OUTGOING", targetEntity="Generation", collection=false)
     * @Groups({"serieInfo"})
     */
    private $generation;
    /**
     * @OGM\Relationship(type="PART_OF", direction="INCOMING", targetEntity="GameVersion", collection=true)
     * @Groups({"serieInfo"})
     */
    private $versions;
    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return Generation
     */
    public function getGeneration(): ?Generation
    {
        return $this->generation;
    }
    /**
     * @param Generation $generation
     */
    public function setGeneration(Generation $generation)
    {
        $this->generation = $generation;
    }
    /**
     * @return GameVersion
     */
    public function getVersions()
    {
        return $this->versions;
    }
    /**
     * @param GameVersion $versions
     */
    public function setVersions($versions)
    {
        $this->versions = $versions;
    }
    /**
     * @return string
     */
    public function getAcronymEn(): string
    {
        return $this->acronym_en;
    }
    /**
     * @param string $acronym_en
     */
    public function setAcronymEn(string $acronym_en)
    {
        $this->acronym_en = $acronym_en;
    }
    /**
     * @return string
     */
    public function getAcronymFr(): string
    {
        return $this->acronym_fr;
    }
    /**
     * @param string $acronym_fr
     */
    public function setAcronymFr(string $acronym_fr)
    {
        $this->acronym_fr = $acronym_fr;
    }
}
