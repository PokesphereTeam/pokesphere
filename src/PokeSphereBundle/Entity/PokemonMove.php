<?php
namespace PokeSphereBundle\Entity;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use GraphAware\Neo4j\OGM\Common\Collection;
use Doctrine\Common\Collections\Collection as ICollection;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * @OGM\Node(label="PokemonMove", repository="PokeSphereBundle\Repository\PokemonMoveRepository")
 */
class PokemonMove
{
    public function __construct(Pokemon $pokemon, Move $move)
    {
        $this->pokemon = $pokemon;
        $this->move    = $move;
        $this->versions = new Collection();
    }
    #region attributes
    /**
     * @OGM\GraphId()
     * @var int
     */
    private $id;
    /**
     * @var Pokemon
     * @OGM\Relationship(type="CAN_LEARN", direction="BOTH", targetEntity="Pokemon", collection=false, mappedBy="pokemonMoves")
     */
    private $pokemon;
    /**
     * @var Move
     * @OGM\Relationship(type="CAN_BE_LEARNED", direction="BOTH", targetEntity="Move", collection=false, mappedBy="pokemonMoves")
     * @Groups({"pokemon_info"})
     */
    private $move;
    /**
     * @var GameVersion
     * @OGM\Relationship(type="CAN_BE_LEARNED_IN_VERSION", direction="BOTH", targetEntity="GameVersion", collection=true, mappedBy="pokemonMoves")
     */
    private $versions;
    /**
     * @var bool
     * @OGM\Property(type="boolean")
     */
    private $isEggmove;
    #endregion
    #region accessors
    public function getId() : int
    {
        return $this->id;
    }
    public function getPokemon() : Pokemon
    {
        return $this->pokemon;
    }
    public function setPokemon(Pokemon $pokemon) : PokemonMove
    {
        $this->pokemon = $pokemon;
        return $this;
    }
    public function getMove() : Move
    {
        return $this->move;
    }
    public function setMove(Move $move) : PokemonMove
    {
        $this->move = $move;
        return $this;
    }
    public function getVersions() : ICollection
    {
        return $this->versions;
    }
    public function setVersion(ICollection $versions) : PokemonMove
    {
        $this->versions = $versions;
        return $this;
    }
    public function getisEggmove()
    {
        return $this->isEggmove;
    }
    public function setIsEggmove($isEggmove)
    {
        $this->isEggmove = $isEggmove;
        return $this;
    }
    #endregion
}
