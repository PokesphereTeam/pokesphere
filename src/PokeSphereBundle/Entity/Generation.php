<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 4/22/2017
 * Time: 9:24 PM
 */
namespace PokeSphereBundle\Entity;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * @OGM\Node(label="Generation")
 */
class Generation
{
    /**
     * @var int
     * @OGM\GraphId()
     */
    private $id;
    /**
     * @var int
     * @OGM\Property(type="int")
     * @Groups({"genInfo"})
     */
    private $genNumber;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * @return mixed
     */
    public function getGenNumber()
    {
        return $this->genNumber;
    }
    /**
     * @param mixed $genNumber
     */
    public function setGenNumber($genNumber)
    {
        $this->genNumber = $genNumber;
    }
}
