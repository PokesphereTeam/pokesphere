<?php
namespace PokeSphereBundle\Entity;
use GraphAware\Neo4j\OGM\Annotations as OGM;
/**
 * @OGM\Node(label="Session")
 * @see Neo4jSessionHandler
 */
class Session
{
    public function __construct($sess_id)
    {
        $this->sess_id = $sess_id;
    }
    #region properties
    /**
     * @var int
     * @OGM\GraphId()
     */
    private $id;
    /**
     * @var string
     * @OGM\Property(type="string")
     */
    private $sess_id;
    /**
     * @var string
     * @OGM\Property(type="string")
     */
    private $sess_data;
    /**
     * @var int
     * @OGM\Property(type="int")
     */
    private $sess_lifetime;
    /**
     * @var int
     * @OGM\Property(type="int")
     */
    private $sess_time;
    #endregion
    #region accessors
    public function getId() : int
    {
        return $this->id;
    }
    public function getSessId() : string
    {
        return $this->sess_id;
    }
    public function setSessId(string $sess_id) : Session
    {
        $this->sess_id = $sess_id;
        return $this;
    }
    public function getSessData() : string
    {
        return $this->sess_data;
    }
    public function setSessData(string $sess_data) : Session
    {
        $this->sess_data = $sess_data;
        return $this;
    }
    public function getSessLifetime() : int
    {
        return $this->sess_lifetime;
    }
    public function setSessLifetime(int $sess_lifetime) : Session
    {
        $this->sess_lifetime = $sess_lifetime;
        return $this;
    }
    public function getSessTime() : int
    {
        return $this->sess_time;
    }
    public function setSessTime(int $sess_time) : Session
    {
        $this->sess_time = $sess_time;
        return $this;
    }
    #endregion
}
