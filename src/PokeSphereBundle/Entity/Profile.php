<?php
/**
 * Created by PhpStorm.
 * User: Sobraz
 * Date: 20/05/2017
 * Time: 09:27
 */
namespace PokeSphereBundle\Entity;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * @OGM\Node(repository="PokeSphereBundle\Repository\ProfileRepository")
 */
class Profile
{
    /**
     * @var boolean
     * @OGM\Label("User")
     * @Groups({"profileInfo"})
     */
    private $isUser = false;
    /**
     * @var boolean
     * @OGM\Label("Community")
     * @Groups({"profileInfo"})
     */
    private $isCommu = false;
    /**
     * @var int
     * @OGM\GraphId()
     * @Groups({"profileInfo"})
     */
    private $id;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Groups({"profileInfo"})
     */
    private $name;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Groups({"profileInfo"})
     */
    private $pseudo;
    /**
     * @var int
     * @OGM\Property(type="int")
     * @Groups({"profileInfo"})
     */
    private $createdAt;
    /**
     * @OGM\Relationship(type="CHOOSE_AVATAR", targetEntity="Avatar", direction="OUTGOING", collection=false)
     * @Groups({"profileInfo"})
     */
    private $avatar;
    /**
     * @return boolean
     */
    public function isIsUser(): bool
    {
        return $this->isUser;
    }
    /**
     * @param boolean $isUser
     */
    public function setIsUser(bool $isUser)
    {
        $this->isUser = $isUser;
    }
    /**
     * @return boolean
     */
    public function isIsCommu(): bool
    {
        return $this->isCommu;
    }
    /**
     * @param boolean $isCommu
     */
    public function setIsCommu(bool $isCommu)
    {
        $this->isCommu = $isCommu;
    }
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @return string
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }
    /**
     * @param string $pseudo
     */
    public function setPseudo(string $pseudo)
    {
        $this->pseudo = $pseudo;
    }
    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->createdAt = $createdAt;
    }
    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }
    /**
     * @param mixed $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }
}
