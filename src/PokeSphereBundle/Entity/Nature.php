<?php
namespace PokeSphereBundle\Entity;
use Doctrine\Common\Collections\Collection as ICollection;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use Symfony\Component\Serializer\Annotation\Groups;
/*
 * load csv WITH HEADERS from "file:///Nature.csv" as line create (tmp:Nature {name_fr: line.name_fr, name_en: line.name_en, attaque: line.attaque, defense: line.defense, attaque_speciale: line.attaque_speciale, defense_speciale: line.defense_speciale, vitesse: line.vitesse}) return tmp
 */
/**
 * @OGM\Node(label="Nature")
 */
class Nature
{
    #region properties
    /**
     * @OGM\GraphId()
     * @Groups({"capture_info", "nature_basic_info"})
     * @var int
     */
    private $id;
    /**
     * @OGM\Property(type="string")
     * @Groups({"nature_basic_info"})
     * @var string
     */
    private $name_fr;
    /**
     * @OGM\Property(type="string")
     * @Groups({"nature_basic_info"})
     * @var string
     */
    private $name_en;
    /**
     * @OGM\Property(type="int")
     * @var integer
     */
    private $attaque;
    /**
     * @OGM\Property(type="int")
     * @var integer
     */
    private $attaque_spe;
    /**
     * @OGM\Property(type="int")
     * @var integer
     */
    private $defense;
    /**
     * @OGM\Property(type="int")
     * @var integer
     */
    private $defense_speciale;
    /**
     * @OGM\Property(type="int")
     * @var integer
     */
    private $vitesse;
    /**
     * @OGM\Relationship(type="HAS_NATURE", direction="BOTH", targetEntity="Capture", collection=true, mappedBy="nature")
     * @var ICollection|Capture[]
     */
    private $captures;
    #endregion
    #region accessors
    public function getId() : int
    {
        return $this->id;
    }
    public function getNameFr() : string
    {
        return $this->name_fr;
    }
    public function setNameFr(string $name_fr) : Nature
    {
        $this->name_fr = $name_fr;
        return $this;
    }
    public function getNameEn() : string
    {
        return $this->name_en;
    }
    public function setNameEn(string $name_en) : Nature
    {
        $this->name_en = $name_en;
        return $this;
    }
    public function getAttaque() : int
    {
        return $this->attaque;
    }
    public function setAttaque(int $attaque) : Nature
    {
        $this->attaque = $attaque;
        return $this;
    }
    public function getAttaqueSpe() : int
    {
        return $this->attaque_spe;
    }
    public function setAttaqueSpe(int $attaque_spe) : Nature
    {
        $this->attaque_spe = $attaque_spe;
        return $this;
    }
    public function getDefense() : int
    {
        return $this->defense;
    }
    public function setDefense(int $defense) : Nature
    {
        $this->defense = $defense;
        return $this;
    }
    public function getDefenseSpeciale() : int
    {
        return $this->defense_speciale;
    }
    public function setDefenseSpeciale(int $defense_speciale) : Nature
    {
        $this->defense_speciale = $defense_speciale;
        return $this;
    }
    public function getVitesse() : int
    {
        return $this->vitesse;
    }
    public function setVitesse(int $vitesse) : Nature
    {
        $this->vitesse = $vitesse;
        return $this;
    }
    public function getCaptures() : ICollection
    {
        return $this->captures;
    }
    public function setCaptures(array $captures) : Nature
    {
        $this->captures = $captures;
        return $this;
    }
    #endregion
}
