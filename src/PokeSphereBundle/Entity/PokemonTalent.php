<?php
namespace PokeSphereBundle\Entity;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * @OGM\RelationshipEntity(type="MAY_HAVE_TALENT")
 */
class PokemonTalent
{
    #region attributes
    /**
     * @OGM\GraphId()
     * @var int
     */
    private $id;
    /**
     * @var Pokemon
     * @OGM\StartNode(targetEntity="Pokemon")
     */
    private $pokemon;
    /**
     * @var Talent
     * @OGM\EndNode(targetEntity="Talent")
     * @Groups({"pokemon_info"})
     */
    private $talent;
    /**
     * @var bool
     * @OGM\Property(type="boolean")
     */
    private $isHidden = false;
    #endregion
    #region accessors
    public function getId(): int
    {
        return $this->id;
    }
    public function setId(int $id): PokemonTalent
    {
        $this->id = $id;
        return $this;
    }
    public function getPokemon()
    {
        return $this->pokemon;
    }
    public function setPokemon($pokemon)
    {
        $this->pokemon = $pokemon;
        return $this;
    }
    public function getTalent()
    {
        return $this->talent;
    }
    public function setTalent($talent)
    {
        $this->talent = $talent;
        return $this;
    }
    public function isIsHidden() : bool
    {
        return $this->isHidden;
    }
    public function setIsHidden(bool $isHidden) : PokemonTalent
    {
        $this->isHidden = $isHidden;
        return $this;
    }
    #endregion
}
