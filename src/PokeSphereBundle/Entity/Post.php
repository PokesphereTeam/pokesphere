<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 5/13/2017
 * Time: 12:55 PM
 */
namespace PokeSphereBundle\Entity;
use DateTime;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @OGM\Node(label="Post", repository="\PokeSphereBundle\Repository\PostRepository")
 */
class Post
{
    /**
     * @var int
     * @OGM\GraphId();
     * @Groups({"postInfo"})
     */
    private $id;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Assert\Length(min="3", max="50",
     *                  minMessage="post.title.length",
     *                  maxMessage="post.title.length")
     * @Groups({"postInfo"})
     */
    private $title;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Assert\Length(min="3", max="500",
     *                  minMessage="post.content.length",
     *                  maxMessage="post.content.length")
     * @Groups({"postInfo"})
     */
    private $content;
    /**
     * @var DateTime
     * @OGM\Property()
     * @OGM\Convert(type="datetime", options={"format":"timestamp"})
     * @Groups({"postInfo"})
     */
    private $createdAt;
    /**
     * @var User
     * @OGM\Relationship(type="WRITTEN_BY", direction="OUTGOING",
     *                  targetEntity="User", collection=false)
     * @Groups({"postInfo"})
     */
    private $writer;
    /**
     * @var bool
     * @OGM\Label("PublicPost")
     * @Assert\NotNull(message="post.public.notnull")
     */
    private $public;
    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }
    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }
    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }
    /**
     * @param string $content
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    }
    /**
     * @return DateTime
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }
    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }
    /**
     * @return User
     */
    public function getWriter(): ?User
    {
        return $this->writer;
    }
    /**
     * @param User $writer
     */
    public function setWriter(User $writer)
    {
        $this->writer = $writer;
    }
    /**
     * @return bool
     */
    public function getPublic(): ?bool
    {
        return $this->public;
    }
    /**
     * @param bool $isPublic
     */
    public function setPublic(bool $isPublic)
    {
        $this->public = $isPublic;
    }
}
