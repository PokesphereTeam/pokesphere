<?php
namespace PokeSphereBundle\Entity;
use Doctrine\Common\Collections\Collection as ICollection;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use GraphAware\Neo4j\OGM\Common\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * @OGM\Node(label="Capture", repository="PokeSphereBundle\Repository\CaptureRepository")
 */
class Capture
{
    public function __construct()
    {
        $this->shiny = false;
        $this->moves = new Collection();
    }
    #region properties
    /**
     * @OGM\GraphId()
     * @Groups({"capture_info"})
     * @var int
     */
    private $id;
    /**
     * @OGM\Property(type="string")
     * @Groups({"capture_info"})
     * @var string
     */
    private $nickname;
    /**
     * @OGM\Property(type="string")
     * @Groups({"capture_info"})
     * @var string
     */
    private $sex;
    /**
     * @OGM\Property(type="boolean")
     * @Groups({"capture_info"})
     * @var boolean
     */
    private $shiny;
    /**
     * @OGM\Relationship(type="INSTANCE_OF", direction="BOTH", targetEntity="Pokemon", collection=false, mappedBy="captures")
     * @Groups({"capture_info"})
     * @var Pokemon
     */
    private $pokemon;
    /**
     * @OGM\Relationship(type="CAUGHT_WITH", direction="BOTH", targetEntity="Ball", collection=false, mappedBy="captures")
     * @Groups({"capture_info"})
     * @var Ball
     */
    private $ball;
    /**
     * @OGM\Relationship(type="CAUGHT_IN", direction="BOTH", targetEntity="Game", collection=false, mappedBy="captures")
     * @Groups({"capture_info"})
     * @var Game
     */
    private $game;
    /**
     * @OGM\Relationship(type="HAS_TALENT", direction="BOTH", targetEntity="Talent", collection=false, mappedBy="captures")
     * @Groups({"capture_info"})
     * @var Talent
     */
    private $talent;
    /**
     * @OGM\Relationship(type="HAS_NATURE", direction="BOTH", targetEntity="Nature", collection=false, mappedBy="captures")
     * @Groups({"capture_info"})
     * @var Nature
     */
    private $nature;
    /**
     * @OGM\Relationship(type="HOLDS", direction="OUTGOING", targetEntity="PokeItem", collection=false)
     * @Groups({"capture_info"})
     * @var PokeItem
     */
    private $pokeItem;
    /**
     * @OGM\Relationship(type="HAS_MOVES", direction="OUTGOING", targetEntity="Move", collection=true)
     * @Groups({"capture_info"})
     * @var ICollection
     */
    private $moves;
    #region IV
    /**
     * @OGM\Relationship(type="HAS_HP_IV",      direction="OUTGOING", targetEntity="IVInterval", collection=false)
     * @Groups({"capture_info"})
     */
    private $hp_iv;
    /**
     * @OGM\Relationship(type="HAS_ATK_IV",     direction="OUTGOING", targetEntity="IVInterval", collection=false)
     * @Groups({"capture_info"})
     */
    private $atk_iv;
    /**
     * @OGM\Relationship(type="HAS_DEF_IV",     direction="OUTGOING", targetEntity="IVInterval", collection=false)
     * @Groups({"capture_info"})
     */
    private $def_iv;
    /**
     * @OGM\Relationship(type="HAS_SPE_ATK_IV", direction="OUTGOING", targetEntity="IVInterval", collection=false)
     * @Groups({"capture_info"})
     */
    private $spe_atk_iv;
    /**
     * @OGM\Relationship(type="HAS_SPE_DEF_IV", direction="OUTGOING", targetEntity="IVInterval", collection=false)
     * @Groups({"capture_info"})
     */
    private $spe_def_iv;
    /**
     * @OGM\Relationship(type="HAS_SPEED_IV",   direction="OUTGOING", targetEntity="IVInterval", collection=false)
     * @Groups({"capture_info"})
     */
    private $speed_iv;
    #endregion
    #region EV
    /**
     * @OGM\Property(type="int")
     * @Groups({"capture_info"})
     * @var int
     */
    private $hp_ev;
    /**
     * @OGM\Property(type="int")
     * @Groups({"capture_info"})
     * @var int
     */
    private $atk_ev;
    /**
     * @OGM\Property(type="int")
     * @Groups({"capture_info"})
     * @var int
     */
    private $def_ev;
    /**
     * @OGM\Property(type="int")
     * @Groups({"capture_info"})
     * @var int
     */
    private $spe_atk_ev;
    /**
     * @OGM\Property(type="int")
     * @Groups({"capture_info"})
     * @var int
     */
    private $spe_def_ev;
    /**
     * @OGM\Property(type="int")
     * @Groups({"capture_info"})
     * @var int
     */
    private $speed_ev;
    #endregion
    #endregion
    #region accessors
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * Please don't call me unless you wanna kill kittens
     */
    public function setId(int $id) : Capture
    {
        $this->id = $id;
        return $this;
    }
    public function getNickname() : ?string
    {
        return $this->nickname;
    }
    public function setNickname(?string $nickname) : Capture
    {
        $this->nickname = $nickname;
        return $this;
    }
    public function getSex() : ?string
    {
        return $this->sex;
    }
    public function setSex(?string $sex) : Capture
    {
        $this->sex = $sex;
        return $this;
    }
    public function getShiny() : bool
    {
        return $this->shiny;
    }
    public function setShiny(bool $shiny) : Capture
    {
        $this->shiny = $shiny;
        return $this;
    }
    public function getPokemon() : Pokemon
    {
        return $this->pokemon;
    }
    public function setPokemon(Pokemon $pokemon) : Capture
    {
        $this->pokemon = $pokemon;
        return $this;
    }
    public function getBall() : Ball
    {
        return $this->ball;
    }
    public function setBall(Ball $ball) : Capture
    {
        $this->ball = $ball;
        return $this;
    }
    public function getGame() : Game
    {
        return $this->game;
    }
    public function setGame(Game $game) : Capture
    {
        $this->game = $game;
        return $this;
    }
    public function getTalent() : ?Talent
    {
        return $this->talent;
    }
    public function setTalent(?Talent $talent) : Capture
    {
        $this->talent = $talent;
        return $this;
    }
    public function getNature() : ?Nature
    {
        return $this->nature;
    }
    public function setNature(?Nature $nature) : Capture
    {
        $this->nature = $nature;
        return $this;
    }
    // FIX ME https://github.com/graphaware/neo4j-php-ogm/issues/141
    public function getPokeItem() // : ?PokeItem
    {
        return $this->pokeItem;
    }
    public function setPokeItem(?PokeItem $pokeItem)
    {
        $this->pokeItem = $pokeItem;
        return $this;
    }
    public function getMoves() : ICollection
    {
        return $this->moves;
    }
    public function setMoves(ICollection $moves) : Capture
    {
        $this->moves = $moves;
        return $this;
    }
    #region IV
    public function getHpIv()
    {
        return $this->hp_iv;
    }
    public function setHpIv($hp_iv)
    {
        $this->hp_iv = $hp_iv;
        return $this;
    }
    public function getAtkIv()
    {
        return $this->atk_iv;
    }
    public function setAtkIv($atk_iv)
    {
        $this->atk_iv = $atk_iv;
        return $this;
    }
    public function getDefIv()
    {
        return $this->def_iv;
    }
    public function setDefIv($def_iv)
    {
        $this->def_iv = $def_iv;
        return $this;
    }
    public function getSpeAtkIv()
    {
        return $this->spe_atk_iv;
    }
    public function setSpeAtkIv($spe_atk_iv)
    {
        $this->spe_atk_iv = $spe_atk_iv;
        return $this;
    }
    public function getSpeDefIv()
    {
        return $this->spe_def_iv;
    }
    public function setSpeDefIv($spe_def_iv)
    {
        $this->spe_def_iv = $spe_def_iv;
        return $this;
    }
    public function getSpeedIv()
    {
        return $this->speed_iv;
    }
    public function setSpeedIv($speed_iv)
    {
        $this->speed_iv = $speed_iv;
        return $this;
    }
    #endregion
    #region EV
    public function getHpEv() : int
    {
        return $this->hp_ev;
    }
    public function setHpEv(int $hp_ev) : Capture
    {
        $this->hp_ev = $hp_ev;
        return $this;
    }
    public function getAtkEv() : int
    {
        return $this->atk_ev;
    }
    public function setAtkEv(int $atk_ev) : Capture
    {
        $this->atk_ev = $atk_ev;
        return $this;
    }
    public function getDefEv() : int
    {
        return $this->def_ev;
    }
    public function setDefEv(int $def_ev) : Capture
    {
        $this->def_ev = $def_ev;
        return $this;
    }
    public function getSpeAtkEv() : int
    {
        return $this->spe_atk_ev;
    }
    public function setSpeAtkEv(int $spe_atk_ev) : Capture
    {
        $this->spe_atk_ev = $spe_atk_ev;
        return $this;
    }
    public function getSpeDefEv() : int
    {
        return $this->spe_def_ev;
    }
    public function setSpeDefEv(int $spe_def_ev) : Capture
    {
        $this->spe_def_ev = $spe_def_ev;
        return $this;
    }
    public function getSpeedEv() : int
    {
        return $this->speed_ev;
    }
    public function setSpeedEv(int $speed_ev) : Capture
    {
        $this->speed_ev = $speed_ev;
        return $this;
    }
    #endregion
    #endregion
}
