<?php
/**
 * Created by PhpStorm.
 * User: Sobraz
 * Date: 05/05/2017
 * Time: 19:31
 */
namespace PokeSphereBundle\Entity;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * @OGM\Node(label="Avatar", repository="PokeSphereBundle\Repository\AvatarRepository")
 */
class Avatar
{
    public const BASE_FOLDER_AVATARS = "/public/images/avatars";
    public const BASE_FOLDER_AVATARS_USERS = self::BASE_FOLDER_AVATARS . "/users";
    public const BASE_FOLDER_AVATARS_COMMUNITIES = self::BASE_FOLDER_AVATARS . "/communities";
    /**
     * @OGM\Label(name="AvatarUser")
     */
    private $isAvatarUser;
    /**
     * @OGM\Label(name="AvatarCommunity")
     */
    private $isAvatarCommunity;
    /**
     * @OGM\GraphId()
     * @Groups({"details","avatarInfo"})
     */
    private $id;
    /**
     * @OGM\Property(type="string")
     * @Groups({"details","avatarInfo"})
     */
    private $url;
    /**
     * @var string
     * @OGM\Property(type="string")
     */
    private $tag;
    /**
     * @OGM\Relationship(type="UPLOADED_BY", targetEntity="User", collection=false)
     */
    private $uploader;
    /**
     * @return mixed
     */
    public function getIsAvatarUser()
    {
        return $this->isAvatarUser;
    }
    /**
     * @param mixed $isAvatarUser
     */
    public function setIsAvatarUser($isAvatarUser)
    {
        $this->isAvatarUser = $isAvatarUser;
    }
    /**
     * @return mixed
     */
    public function getIsAvatarCommunity()
    {
        return $this->isAvatarCommunity;
    }
    /**
     * @param mixed $isAvatarCommunity
     */
    public function setIsAvatarCommunity($isAvatarCommunity)
    {
        $this->isAvatarCommunity = $isAvatarCommunity;
    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }
    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }
    /**
     * @return mixed
     */
    public function getUploader()
    {
        return $this->uploader;
    }
    /**
     * @param mixed $uploader
     */
    public function setUploader($uploader)
    {
        $this->uploader = $uploader;
    }
    /**
     * @return string
     */
    public function getTag(): string
    {
        return $this->tag;
    }
    /**
     * @param string $tag
     */
    public function setTag(string $tag)
    {
        $this->tag = $tag;
    }
}
