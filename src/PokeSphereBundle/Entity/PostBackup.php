<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 5/13/2017
 * Time: 6:50 PM
 */
namespace PokeSphereBundle\Entity;
use DateTime;
use GraphAware\Neo4j\OGM\Annotations as OGM;
/**
 * @OGM\Node(label="PostBackup")
 */
class PostBackup
{
    /**
     * @var int
     * @OGM\GraphId()
     */
    private $id;
    /**
     * @var string
     * @OGM\Property(type="string")
     */
    private $title;
    /**
     * @var string
     * @OGM\Property(type="string")
     */
    private $content;
    /**
     * @var bool
     * @OGM\Property(type="boolean")
     */
    private $isPublic;
    /**
     * @var DateTime
     * @OGM\Property()
     * @OGM\Convert(type="datetime", options={"format":"long_timestamp"})
     */
    private $createdAt;
    /**
     * @var Post
     * @OGM\Relationship(type="HYSTORIZED", direction="INCOMING",
     *                  targetEntity="Post", collection=false)
     */
    private $post;
    public static function createFromPost(Post $post): PostBackup{
        $backup = new PostBackup();
        $backup->setTitle($post->getTitle());
        $backup->setContent($post->getContent());
        $backup->setPost($post);
        $backup->setPublic($post->getPublic()?true:false);
        return $backup;
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }
    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
    /**
     * @param string $content
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    }
    /**
     * @return mixed
     */
    public function getPost()
    {
        return $this->post;
    }
    /**
     * @param mixed $post
     */
    public function setPost($post)
    {
        $this->post = $post;
    }
    /**
     * @return DateTime
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }
    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }
    /**
     * @return bool
     */
    public function isPublic(): bool
    {
        return $this->isPublic;
    }
    /**
     * @param bool $isPublic
     */
    public function setPublic(bool $isPublic)
    {
        $this->isPublic = $isPublic;
    }
}
