<?php
namespace PokeSphereBundle\Entity;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use GraphAware\Neo4j\OGM\Common\Collection;
use Doctrine\Common\Collections\Collection as ICollection;
use Symfony\Component\Serializer\Annotation\Groups;
/*
 * CREATE CONSTRAINT ON (entity:Pokemon) ASSERT entity.poketoolsId IS UNIQUE;
 */
/**
 * @OGM\Node(label="Pokemon", repository="PokeSphereBundle\Repository\PokemonRepository")
 */
class Pokemon
{
    public function __construct(int $poketoolsId, int $numDex = null)
    {
        $this->evolutions     = new Collection();
        $this->formes         = new Collection();
        $this->types          = new Collection();
        $this->pokemonTalents = new Collection();
        $this->pokemonMoves   = new Collection();
        $this->numDex         = $numDex;
        $this->poketoolsId    = $poketoolsId;
    }
    #region Attributes
    /**
     * @OGM\GraphId()
     * @Groups({"capture_info", "pokemon_basic_info", "pokemon_info"})
     * @var int
     */
    private $id;
    /**
     * @OGM\Property(type="int", nullable=false)
     * @Groups({"pokemon_info"})
     * @var int
     */
    private $numDex;
    /**
     * @OGM\Property(type="int", nullable=false)
     * @var int
     */
    private $poketoolsId;
    /**
     * @OGM\Property(type="string")
     * @Groups({"pokemon_basic_info", "pokemon_info"})
     * @var string
     */
    private $name;
    /**
     * @OGM\Property(type="string")
     * @Groups({"pokemon_basic_info", "pokemon_info"})
     * @var string
     */
    private $name_fr;
    /**
     * @OGM\Relationship(type="ENVOLVES_INTO", direction="INCOMING", relationshipEntity="Evolution", collection=false, mappedBy="antecedant")
     * @var Evolution
     */
    private $antecedant;
    /**
     * @OGM\Relationship(type="ENVOLVES_INTO", direction="OUTGOING", relationshipEntity="Evolution", collection=true, mappedBy="evolution")
     * @var ICollection | Pokemon[]
     */
    private $evolutions;
    /**
     * @OGM\Relationship(type="MAY_HAVE_FORM", direction="BOTH", targetEntity="Pokemon", collection=true)
     * @var ICollection | Pokemon[]
     */
    private $formes;
    /**
     * @OGM\Relationship(type="IS_OF_TYPE", direction="OUTGOING", targetEntity="Type", collection=true, mappedBy="pokemons")
     * @Groups({"pokemon_info"})
     * @var ICollection | Type[]
     */
    private $types;
    /**
     * @OGM\Relationship(type="MAY_HAVE_TALENT", direction="OUTGOING", relationshipEntity="PokemonTalent", collection=true, mappedBy="pokemon")
     * @Groups({"pokemon_info"})
     * @var ICollection | PokemonTalent[]
     */
    private $pokemonTalents;
    /**
     * @OGM\Relationship(type="CAN_LEARN", direction="BOTH", targetEntity="PokemonMove", collection=true, mappedBy="pokemon")
     * @Groups({"pokemon_info"})
     * @return ICollection | PokemonMove[]
     */
    private $pokemonMoves;
    /**
     * @OGM\Relationship(type="INSTANCE_OF", direction="BOTH", targetEntity="Capture", collection=true, mappedBy="pokemon")
     * @var ICollection|Capture[]
     */
    private $captures;
    #endregion
    #region accessors
    public function getId(): int
    {
        return $this->id;
    }
    public function getName() : string
    {
        return $this->name;
    }
    public function setName(string $name)
    {
        $this->name = $name;
    }
    public function getTypes() : ICollection
    {
        return $this->types;
    }
    public function getNumDex() : int
    {
        return $this->numDex;
    }
    public function setNumDex(int $numDex) : Pokemon
    {
        $this->numDex = $numDex;
        return $this;
    }
    public function getPoketoolsId() : int
    {
        return $this->poketoolsId;
    }
    public function getAntecedant() : ?Evolution
    {
        return $this->antecedant;
    }
    public function setAntecedant(Evolution $antecedant) : Pokemon
    {
        $this->antecedant = $antecedant;
        return $this;
    }
    /**
     * @return Collection | Evolution[]
     */
    public function getEvolutions() : ICollection
    {
        return $this->evolutions;
    }
    public function setEvolutions(ICollection $evolutions) : Pokemon
    {
        $this->evolutions = $evolutions;
        return $this;
    }
    public function getFormes() : ICollection
    {
        return $this->formes;
    }
    public function setFormes($formes)
    {
        $this->formes = $formes;
        return $this;
    }
    public function getNameFr() : string
    {
        return $this->name_fr;
    }
    public function setNameFr(string $name_fr) : Pokemon
    {
        $this->name_fr = $name_fr;
        return $this;
    }
    /** @return PokemonTalent[] */
    public function getPokemonTalents() : ICollection
    {
        return $this->pokemonTalents;
    }
    public function setPokemonTalents(ICollection $pokemonTalents) : Pokemon
    {
        $this->pokemonTalents = $pokemonTalents;
        return $this;
    }
    /**
     * @return Collection | PokemonMove[]
     */
    public function getPokemonMoves() : ICollection
    {
        return $this->pokemonMoves;
    }
    public function setPokemonMoves(ICollection $pokemonMoves) : Pokemon
    {
        $this->pokemonMoves = $pokemonMoves;
        return $this;
    }
    public function getCaptures() : ICollection
    {
        return $this->captures;
    }
    public function setCaptures(ICollection $captures) : Pokemon
    {
        $this->captures = $captures;
        return $this;
    }
    #endregion
    public function getSpriteUrl(string $variante = null) : string
    {
        if (! empty($variante))
            $variante = "-$variante";
        return '/images/pokemon/thumbnails/'.$this->getNumDex()."$variante.png";
    }
    public function getPokepediaUrl() : string
    {
        return 'http://www.pokepedia.fr/Pokémon_n°'.$this->getNumDex();
    }
    public function getPoketoolsUrl($absolute=true) : string
    {
        if (! $absolute)
            return '/pokemon/'.$this->getPoketoolsId().'-x.html';
        return 'https://www.poketools.fr/pokemon/'.$this->getPoketoolsId().'-x.html';
    }
}
