<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 4/13/2017
 * Time: 1:54 PM
 */
namespace PokeSphereBundle\Entity;
use Doctrine\Common\Annotations\Annotation\Enum;
use GraphAware\Neo4j\OGM\Annotations as OGM;
/**
 * @OGM\Node(label="ActivationRequest", repository="PokeSphereBundle\Repository\ActivationRequestRepository")
 */
class ActivationRequest extends UpdateRequest
{
    /**
     * @OGM\GraphId()
     * @var int
     */
    private $id;
    /**
     * @var string
     * @OGM\Property(type="string")
     */
    private $token;
    /**
     * @var int
     * @OGM\Property(type="int")
     */
    private $createdAt;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Enum({"WAITTING","CANCELED","EXPIRED","VALIDATED"})
     */
    private $statut;
    /**
     * @var User
     * @OGM\Relationship(targetEntity="User", direction="INCOMING", type="MAKE_REQUEST", collection=false)
     */
    private $user;
    #region Accessors
    public function getId()
    {
        return $this->id;
    }
    public function setId($id) : void
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }
    /**
     * @param string $token
     */
    public function setToken(string $token)
    {
        $this->token = $token;
    }
    /**
     * @return int
     */
    public function getCreatedAt(): ?int
    {
        return $this->createdAt;
    }
    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt)
    {
        $this->createdAt = $createdAt;
    }
    /**
     * @return string
     */
    public function getStatut(): string
    {
        return $this->statut;
    }
    /**
     * @param string $statut
     */
    public function setStatut(string $statut)
    {
        $this->statut = $statut;
    }
    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }
    #endregion
}
