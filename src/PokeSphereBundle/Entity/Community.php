<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 5/7/2017
 * Time: 10:56 PM
 */
namespace PokeSphereBundle\Entity;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @OGM\Node(label="Community")
 */
class Community
{
    /**
     * @OGM\GraphId()
     */
    private $id;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Assert\NotNull(message="community.name.required", groups={"details"})
     * @Assert\Length(min="4", max="15", minMessage="community.name.length", maxMessage="user.pseudo.length", groups={"details"})
     * @Assert\Regex(pattern="/^([a-zA-Z]|\d)*$/i", message="community.name.pattern", groups={"datails"})
     * @Groups({"commuInfo"})
     */
    private $name;
    /**
     * @var string
     * @OGM\Property(type="string", nullable=true)
     * @Assert\Length(max="200", maxMessage="community.description.maxlength", groups={"details"})
     * @Groups({"commuInfo"})
     */
    private $description;
    /**
     * @var Avatar
     * @OGM\Relationship(type="CHOOSE_AVATAR", direction="OUTGOING", targetEntity="Avatar", collection=false)
     * @Assert\NotNull(message="community.avatar.required", groups={"details"})
     * @Groups({"commuInfo"})
     */
    private $avatar;
    /**
     * @var Avatar[]
     * @OGM\Relationship(type="CAN_USE", direction="OUTGOING", targetEntity="Avatar", collection=true)
     */
    private $extraAvatars;
    /**
     * @var User
     * @OGM\Relationship(type="OWNED_BY", direction="OUTGOING", targetEntity="User", collection=false)
     * @Groups({"commuInfo"})
     */
    private $owner;
    /**
     * @OGM\Relationship(type="MANAGED_BY", direction="OUTGOING", targetEntity="User", collection=true, mappedBy="managedCommunities")
     */
    private $staff;
    /**
     * @OGM\Relationship(type="FOLLOW", direction="INCOMING", targetEntity="User", collection=true, mappedBy="followedCommunities")
     */
    private $followers;
    #region Accessors
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    public function getName()
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }
    public function getExtraAvatars()
    {
        return $this->extraAvatars;
    }
    /**
     * @param Avatar[] $extraAvatars
     */
    public function setExtraAvatars(array $extraAvatars)
    {
        $this->extraAvatars = $extraAvatars;
    }
    /**
     * @return User
     */
    public function getOwner() : User
    {
        return $this->owner;
    }
    /**
     * @param User $owner
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;
    }
    /**
     * @return Avatar
     */
    public function getAvatar()
    {
        return $this->avatar;
    }
    /**
     * @param Avatar $avatar
     */
    public function setAvatar(Avatar $avatar)
    {
        $this->avatar = $avatar;
    }
    /**
     * @return mixed
     */
    public function getStaff()
    {
        return $this->staff;
    }
    /**
     * @param mixed $staff
     */
    public function setStaff($staff)
    {
        $this->staff = $staff;
    }
    public function getStaffUser($userid) : ?User{
        foreach ($this->getStaff() as $user)
            if($user->getId() == $userid)
                return $user;
        return null;
    }
    /**
     * @return mixed
     */
    public function getFollowers()
    {
        return $this->followers;
    }
    /**
     * @param mixed $followers
     */
    public function setFollowers($followers)
    {
        $this->followers = $followers;
    }
    #endregion
    public function hasStaff($userid): bool
    {
        foreach($this->getStaff() as $user){
            if($user->getId() == $userid)
                return true;
        }
        return false;
    }
}
