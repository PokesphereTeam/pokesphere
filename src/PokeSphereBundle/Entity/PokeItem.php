<?php
namespace PokeSphereBundle\Entity;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * @OGM\Node(label="PokeItem")
 */
class PokeItem
{
    #region properties
    /**
     * @OGM\GraphId()
     * @Groups({"capture_info"})
     * @var int
     */
    private $id;
    /**
     * @OGM\Property(type="string")
     * @var string
     */
    private $name_fr;
    /**
     * @OGM\Property(type="string")
     * @var string
     */
    private $name_en;
    #endregion
    #region accessors
    public function getId() : int
    {
        return $this->id;
    }
    public function getNameFr() : string
    {
        return $this->name_fr;
    }
    public function setNameFr(string $name_fr) : PokeItem
    {
        $this->name_fr = $name_fr;
        return $this;
    }
    public function getNameEn() : string
    {
        return $this->name_en;
    }
    public function setNameEn(string $name_en) : PokeItem
    {
        $this->name_en = $name_en;
        return $this;
    }
    #endregion
}
