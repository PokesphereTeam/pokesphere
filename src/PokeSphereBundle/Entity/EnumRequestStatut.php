<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 4/13/2017
 * Time: 3:34 PM
 */
namespace PokeSphereBundle\Entity;
class EnumRequestStatut
{
    public const WAITING = "WAITING";
    public const CANCELED = "CANCELED";
    public const EXPIRED = "EXPIRED";
    public const VALIDATED = "VALIDATED";
}

