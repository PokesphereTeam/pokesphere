<?php
namespace PokeSphereBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Collection as ICollection;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use Symfony\Component\Serializer\Annotation\Groups;
/*
 * CREATE CONSTRAINT ON (entity:Talent) ASSERT entity.poketoolsId IS UNIQUE;
 */
/**
 * @OGM\Node(label="Talent")
 */
class Talent
{
    public function __construct()
    {
        $this->pokemons = new ArrayCollection();
    }
    #region properties
    /**
     * @OGM\GraphId()
     * @Groups({"capture_info", "pokemon_info"})
     * @var int
     */
    private $id;
    /**
     * @var int
     * @OGM\Property(type="int")
     */
    private $poketoolsId;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Groups({"pokemon_info"})
     */
    private $name;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Groups({"pokemon_info"})
     */
    private $name_fr;
    /**
     * @var Collection
     * @OGM\Relationship(type="MAY_HAVE_TALENT", direction="INCOMING", relationshipEntity="PokemonTalent", collection=true, mappedBy="talent")
     */
    private $pokemons;
    /**
     * @OGM\Relationship(type="HAS_TALENT", direction="BOTH", targetEntity="Capture", collection=true, mappedBy="talent")
     * @var ICollection|Capture[]
     */
    private $captures;
    #endregion
    #region accessors
    public function getId() : int
    {
        return $this->id;
    }
    public function getPoketoolsId() : int
    {
        return $this->poketoolsId;
    }
    public function setPoketoolsId(int $poketoolsId) : Talent
    {
        $this->poketoolsId = $poketoolsId;
        return $this;
    }
    public function getName() : string
    {
        return $this->name;
    }
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }
    public function getNameFr()
    {
        return $this->name_fr;
    }
    public function setNameFr($name_fr)
    {
        $this->name_fr = $name_fr;
        return $this;
    }
    public function getPokemons() : Collection
    {
        return $this->pokemons;
    }
    public function setPokemons(Collection $pokemons) : Talent
    {
        $this->pokemons = $pokemons;
        return $this;
    }
    public function getCaptures() : ICollection
    {
        return $this->captures;
    }
    public function setCaptures(ICollection $captures) : Talent
    {
        $this->captures = $captures;
        return $this;
    }
    #endregion
    public static function getPoketoolsUrl(int $id) : string
    {
        return 'https://www.poketools.fr/talent/'.$id.'-x.html';
    }
}
