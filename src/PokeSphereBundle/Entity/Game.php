<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 4/22/2017
 * Time: 9:23 PM
 */
namespace PokeSphereBundle\Entity;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use Doctrine\Common\Collections\Collection as ICollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @OGM\Node(label="Game", repository="PokeSphereBundle\Repository\GameRepository")
 */
class Game
{
    /**
     * @var int
     * @OGM\GraphId()
     * @Groups({"game_info", "capture_info"})
     */
    private $id;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Groups({"game_info"})
     * @Assert\Length(min="1", max="12", minMessage="game.pseudo.length.min", maxMessage="game.pseudo.length.max")
     */
    private $pseudo;
    /**
     * @var GameVersion
     * @Groups({"game_info"})
     * @OGM\Relationship(type="PRODUCT_OF", direction="OUTGOING", targetEntity="GameVersion", collection=false)
     * @Assert\NotNull(message="game.version.notnulll")
     */
    private $gameVersion;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Groups({"game_info"})
     * @Assert\Length(max="100", maxMessage="game.comment.length.max")
     */
    private $comment;
    /**
     * @var User
     * @OGM\Relationship(type="PLAY_WITH", direction="INCOMING", targetEntity="User", mappedBy="games", collection=false)
     */
    private $user;
    /**
     * @OGM\Relationship(type="CAUGHT_IN", direction="BOTH", targetEntity="Capture", collection=true, mappedBy="game")
     * @Groups({"game_info"})
     * @var ICollection|Capture[]
     */
    private $captures;
    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }
    /**
     * @param string $pseudo
     */
    public function setPseudo(string $pseudo)
    {
        $this->pseudo = $pseudo;
    }
    /**
     * @return GameVersion
     */
    public function getGameVersion()
    {
        return $this->gameVersion;
    }
    /**
     * @param GameVersion $gameVersion
     */
    public function setGameVersion(GameVersion $gameVersion)
    {
        $this->gameVersion = $gameVersion;
    }
    /**
     * @return string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }
    /**
     * @param string $comment
     */
    public function setComment(string $comment)
    {
        $this->comment = $comment;
    }
    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }
    /**
     * @param User $user
     */
    public function setUser(?User $user)
    {
        $this->user = $user;
    }
    public function getCaptures() : ICollection
    {
        return $this->captures;
    }
    public function setCaptures(ICollection $captures) : Game
    {
        $this->captures = $captures;
        return $this;
    }
}
