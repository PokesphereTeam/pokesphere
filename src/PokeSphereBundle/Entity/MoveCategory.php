<?php
namespace PokeSphereBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use GraphAware\Neo4j\OGM\Annotations as OGM;
/*
 * CREATE CONSTRAINT ON (entity:MoveCategory) ASSERT entity.name IS UNIQUE;
 */
/**
 * @OGM\Node(label="MoveCategory")
 */
class MoveCategory
{
    public function __construct($name = null)
    {
        $this->setName($name);
        $this->moves = new ArrayCollection();
    }
    #region attributes
    /**
     * @OGM\GraphId()
     * @var int
     */
    private $id;
    /** @OGM\Property(type="string") */
    private $name;
    /**
     * @var Collection
     * @OGM\Relationship(type="IS_OF_MOVE_CATEGORY", direction="OUTGOING", targetEntity="Move", collection=true, mappedBy="moveCategory")
     */
    private $moves;
    #endregion
    #region accessors
    public function getId() : int
    {
        return $this->id;
    }
    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
        if (! in_array($name, ['physical', 'special', 'status']))
            throw new \Exception("Invalid move name $name");
        $this->name = $name;
        return $this;
    }
    public function getMoves() : Collection
    {
        return $this->moves;
    }
    public function setMoves(Collection $moves) : MoveCategory
    {
        $this->moves = $moves;
        return $this;
    }
    #endregion
}
