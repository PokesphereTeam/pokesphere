<?php
namespace PokeSphereBundle\Entity;
use GraphAware\Neo4j\OGM\Annotations as OGM;
/**
 * @OGM\RelationshipEntity(type="ENVOLVES_INTO")
 *
 * This relationship entity is useless for now but will permit to add evolution level and evolution stone
 * in the future.
 */
class Evolution
{
    #region attributes
    /**
     * @OGM\GraphId()
     * @var int
     */
    private $id;
    /**
     * @OGM\StartNode(targetEntity="Pokemon")
     */
    private $antecedant;
    /**
     * @OGM\EndNode(targetEntity="Pokemon")
     */
    private $evolution;
    #endregion
    #region accessors
    public function getId(): int
    {
        return $this->id;
    }
    public function setId(int $id): Evolution
    {
        $this->id = $id;
        return $this;
    }
    public function getAntecedant()
    {
        return $this->antecedant;
    }
    public function setAntecedant($antecedant)
    {
        $this->antecedant = $antecedant;
        return $this;
    }
    public function getEvolution()
    {
        return $this->evolution;
    }
    public function setEvolution($evolution)
    {
        $this->evolution = $evolution;
        return $this;
    }
    #endregion
}
