<?php
/**
 * Created by PhpStorm.
 * User: Sobraz
 * Date: 16/03/2017
 * Time: 17:04
 */
namespace PokeSphereBundle\Entity;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
class SecurityUser extends User implements AdvancedUserInterface, \Serializable
{
    public function __construct(User $user)
    {
        parent:: __construct();
        $this->setEmail($user->getEmail());
        $this->setId($user->getId());
        $this->setPseudo($user->getPseudo());
        $this->setPassword($user->getPassword());
        $this->setCreatedAt($user->getCreatedAt());
        $this->setIsActive($user->getIsActive());
        $this->setIsValidated($user->getIsValidated());
        $this->setAppRoles($user->getAppRoles());
        $this->setDateOfBirth($user->getDateOfBirth());
        $this->setFriendCode($user->getFriendCode());
        $this->setIsAMan($user->isAMan());
        $this->setAvatar($user->getAvatar());
        $this->setDescription($user->getDescription());
    }
    #region AdvancedUserInterface
    public function getSalt()
    {
        return '';
    }
    public function getRoles()
    {
        return array_merge($this->getAppRoles()?$this->getAppRoles()->toArray():[], ['ROLE_USER']);
    }
    public function getUsername()
    {
        return $this->getPseudo();
    }
    public function eraseCredentials()
    {
    }
    public function isAccountNonExpired()
    {
        return true;
    }
    public function isAccountNonLocked()
    {
        return $this->getIsActive();
    }
    public function isCredentialsNonExpired()
    {
        return true;
    }
    public function isEnabled()
    {
        return $this->getIsValidated();
    }
    #endregion
    #region Serializable
    public function serialize()
    {
        return serialize(array($this->getId()));
    }
    public function unserialize($serialized)
    {
        list($id) = unserialize($serialized);
        $this->setId($id);
    }
    #endregion
}
