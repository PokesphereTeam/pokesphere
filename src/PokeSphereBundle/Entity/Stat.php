<?php
namespace PokeSphereBundle\Entity;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * @OGM\Node(label="Stat", repository="PokeSphereBundle\Repository\StatRepository")
 */
class Stat
{
    /**
     * @var int
     * @OGM\GraphId()
     * @Groups({"stat_info"})
     */
    private $id;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Groups({"stat_info"})
     */
    private $site;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Groups({"stat_info"})
     */
    private $pokemon;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Groups({"stat_info"})
     */
    private $url;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Groups({"stat_info"})
     */
    private $info;
    /**
     * @var int
     * @OGM\Property(type="int")
     * @Groups({"stat_info"})
     */
    private $timestamp;
    /**
     * @OGM\Relationship(type="APPROVED", direction="INCOMING", targetEntity="User", collection=true, mappedBy="approvedStats")
     * @Groups({"stat_info"})
     */
    private $approvers = [];
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getSite(): string
    {
        return $this->site;
    }
    /**
     * @param string $site
     */
    public function setSite(string $site)
    {
        $this->site = $site;
    }
    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
    /**
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }
    /**
     * @return array
     */
    public function getInfo(): array
    {
        return unserialize($this->info);
    }
    /**
     * @param array $info
     */
    public function setInfo(array $info)
    {
        $this->info = serialize($info);
    }
    /**
     * @return int
     */
    public function getTimestamp(): int
    {
        return $this->timestamp;
    }
    /**
     * @param float $timestamp
     */
    public function setTimestamp(float $timestamp)
    {
        $this->timestamp = $timestamp;
    }
    /**
     * @return string
     */
    public function getPokemon(): string
    {
        return $this->pokemon;
    }
    /**
     * @param string $pokemon
     */
    public function setPokemon(string $pokemon)
    {
        $this->pokemon = $pokemon;
    }
    /**
     * @return User[]
     */
    public function getApprovers()
    {
        return $this->approvers;
    }
    /**
     * @param User[] $approvers
     */
    public function setApprovers($approvers)
    {
        $this->approvers = $approvers;
    }
}
