<?php
namespace PokeSphereBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use Symfony\Component\Serializer\Annotation\Groups;
/*
 * CREATE CONSTRAINT ON (entity:Type) ASSERT entity.numDex IS UNIQUE;
 */
/**
 * @OGM\Node(label="Type")
 */
class Type
{
    public function __construct()
    {
        $this->halfDmgTo   = new ArrayCollection();
        $this->doubleDmgTo = new ArrayCollection();
        $this->noDmgTo     = new ArrayCollection();
        $this->pokemons    = new ArrayCollection();
    }
    #region Attributes
    /**
     * @OGM\GraphId()
     * @Groups({"pokemon_info"})
     * @var int
     */
    private $id;
    /**
     * @var int
     * @OGM\Property(type="int")
     */
    private $pokeapi_id;
    /**
     * @var string
     * @OGM\Property(type="string")
     * @Groups({"pokemon_info"})
     */
    private $name;
    /**
     * @var Collection
     * @OGM\Relationship(type="HALF_DMG_TO", direction="OUTGOING", targetEntity="Type", collection=true)
     */
    private $halfDmgTo;
    /**
     * @var Collection
     * @OGM\Relationship(type="DOUBLE_DMG_TO", direction="OUTGOING", targetEntity="Type", collection=true)
     */
    private $doubleDmgTo;
    /**
     * @var Collection
     * @OGM\Relationship(type="NO_DMG_TO", direction="OUTGOING", targetEntity="Type", collection=true)
     */
    private $noDmgTo;
    /**
     * @var Collection
     * @OGM\Relationship(type="IS_OF_TYPE", direction="INCOMING", collection=true, mappedBy="types", targetEntity="Pokemon")
     */
    private $pokemons;
    /**
     * @var Collection
     * @OGM\Relationship(type="IS_OF_TYPE", direction="INCOMING", collection=true, mappedBy="type", targetEntity="Move")
     */
    private $moves;
    #endregion
    #region accessors
    public function getId() : int
    {
        return $this->id;
    }
    public function getPokeapiId(): int
    {
        return $this->pokeapi_id;
    }
    public function setPokeapiId(int $pokeapi_id): Type
    {
        $this->pokeapi_id = $pokeapi_id;
        return $this;
    }
    public function getName() : string
    {
        return $this->name;
    }
    public function setName(string $name) : Type
    {
        $this->name = $name;
        return $this;
    }
    public function getHalfDmgTo() : Collection
    {
        return $this->halfDmgTo;
    }
    public function getDoubleDmgTo() : Collection
    {
        return $this->doubleDmgTo;
    }
    public function getNoDmgTo() : Collection
    {
        return $this->noDmgTo;
    }
    public function getPokemons() : Collection
    {
        return $this->pokemons;
    }
    public function setPokemons(Collection $pokemons) : Type
    {
        $this->pokemons = $pokemons;
        return $this;
    }
    public function getMoves() : Collection
    {
        return $this->moves;
    }
    public function setMoves(Collection $moves) : Type
    {
        $this->moves = $moves;
        return $this;
    }
    #endregion
    public function hydrateFromArray(array $data) : Type
    {
        $this->pokeapi_id  = $data['id'];
        $this->name        = $data['name'];
        return $this;
    }
    public function getPokeapiUrl() : string
    {
        return self::getPokeapiUrlFromId($this->getPokeapiId());
    }
    public static function getPokeapiUrlFromId(int $id) : string
    {
        return "http://pokeapi.co/api/v2/type/".$id."/";
    }
    public static function getPokeapiIdFromPoketoolsId($poketoolsId) : int
    {
        switch ($poketoolsId)
        {
            case  1: return  9; // steel
            case  2: return  3; // fighting
            case  3: return 16; // dragoon
            case  4: return 11; // water
            case  5: return 13; // electric
            case  6: return 18; // fairy
            case  7: return 10; // fire
            case  8: return 15; // ice
            case  9: return  7; // bug
            case 10: return  1; // normal
            case 11: return 12; // grass
            case 12: return  4; // poison
            case 13: return 14; // psychic
            case 14: return  6; // rock
            case 15: return  5; // ground
            case 16: return  8; // ghost
            case 17: return 17; // dark
            case 18: return  3; // flying
            default: throw new \Exception("Unknown type $poketoolsId");
        }
    }
}
