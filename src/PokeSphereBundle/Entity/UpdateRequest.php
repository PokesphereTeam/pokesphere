<?php
/**
 * Created by PhpStorm.
 * User: Sobraz
 * Date: 12/04/2017
 * Time: 23:07
 */
namespace PokeSphereBundle\Entity;
abstract class UpdateRequest
{
    public const EXPIRATION_HOUR_REQUEST = 1;
    public const TOKEN_LENGTH = 6;
    public function __construct()
    {
        $this->setToken(str_replace(array('/', '+', '='), 0,base64_encode(random_bytes(self::TOKEN_LENGTH))));
        $this->setStatut(EnumRequestStatut::WAITING);
        $datetime = new \DateTime("NOW", new \DateTimeZone("UTC"));
        $this->setCreatedAt($datetime->getTimestamp());
    }
    public abstract function getCreatedAt(): ?int;
    public abstract function setCreatedAt(int $createAt);
    public abstract function getToken(): string;
    public abstract function setToken(string $token);
    public abstract function getStatut(): string;
    public abstract function setStatut(string $token);
    public abstract function getUser(): User;
    public abstract function setUser(User $user);
}
