<?php
namespace PokeSphereBundle\Service\Voter;
use PokeSphereBundle\Entity\Capture;
use PokeSphereBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
class CaptureVoter extends Voter
{
    const EDIT = 'edit';
    const DELETE = 'delete';
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::EDIT, self::DELETE)))
            return false;
        if (!$subject instanceof Capture)
            return false;
        return true;
    }
    /**
     * @param string  $attribute
     * @param Capture $subject
     * @param TokenInterface $token
     * @return bool true if granted
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        if ($attribute === self::EDIT || $attribute === self::DELETE)
        {
            $user = $token->getUser();
            if (! $user instanceof User)
                return false;
            if ($subject->getGame()->getUser()->getId() !== $user->getId())
                return false;
            return true;
        }
        return false;
    }
}
