<?php
namespace PokeSphereBundle\Service\Search;
use PokeSphereBundle\Entity\Stat;
use Symfony\Component\DomCrawler\Crawler;
abstract class SearchSlave implements SearchInterface
{
    protected $baseUrl;
    protected $stats;
    /**
     * @var Crawler
     */
    protected $crawler;
    public function __construct($url)
    {
        $this->baseUrl = $url;
        $this->stats = new Stat();
    }
    /**
     * @param string $pokemon
     */
    protected function crawlerPokemon(string $pokemon)
    {
        $uri = $this->baseUrl . $pokemon;
        $this->crawler = new Crawler();
        $this->crawler->add(file_get_contents($uri));
    }
    /**
     * Use SearchSlave to complete search strat file. Then merge and save its.
     *
     * @param $pokemon
     * @return Stat
     */
    abstract public function search($pokemon): Stat;
    /**
     * @param string $pokemon
     */
    public function getInfo(string $pokemon) {
        $this->crawlerPokemon($pokemon);
        $this->stats->setTimestamp(time());
    }
}
