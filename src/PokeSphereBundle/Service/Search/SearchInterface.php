<?php
namespace PokeSphereBundle\Service\Search;
interface SearchInterface
{
    public function search($pokemon);
}
