<?php
namespace PokeSphereBundle\Service\Search\Slave;
use PokeSphereBundle\Entity\Stat;
use PokeSphereBundle\Service\Search\SearchSlave;
use Symfony\Component\DomCrawler\Crawler;
class Pokestrat extends SearchSlave
{
    private const SITE_NAME = 'pokestrat';
    /**
     * Use SearchSlave to complete search strat file. Then merge and save its.
     *
     * @param $pokemon
     * @return Stat
     */
    public function search($pokemon): Stat
    {
        $this->stats->setSite(self::SITE_NAME);
        $this->stats->setUrl($this->baseUrl . $pokemon);
        $this->stats->setInfo($this->getInfo($pokemon));
        $this->stats->setPokemon($pokemon);
        return $this->stats;
    }
    public function getInfo(string $pokemon)
    {
        parent::getInfo($pokemon);
        $stats = $this->crawler
            ->filter('.tableau-stat')
            ->each((function (Crawler $node) {
                $stat = $node->filter('tr')
                    ->each(function (Crawler $node) {
                        $key = $node->filter('td')->first()->text();
                        $value = $node->filter('td b')->last()->text();
                        return [$key => $value];
                    });
                return $stat;
            }));
        $stats['extra'] = $this->getExtraInformation();
        return $stats;
    }
    private function getExtraInformation()
    {
        return $this->crawler
            ->filter('.information_generale')
            ->each(function (Crawler $node) {
                $extra = $node->filter('.partie')->first()->children()
                    ->each(function (Crawler $node, $index) {
                        switch ($index) {
                            case 0:
                                return ['type' => $this->getExtraTypeInformation($node),];
                            case 1:
                                return ['talent' => $this->getExtraTalentInformation($node)];
                            case 2:
                                return ['EV' => $this->getExtraEVInformation($node)];
                            default:
                                break;
                        }
                    });
                return $extra;
            });
    }
    private function getExtraTypeInformation(Crawler $node)
    {
        return $node->filter('a img')->each(function (Crawler $node){
            return $node->attr('alt');
        });
    }
    private function getExtraTalentInformation(Crawler $node)
    {
        return $node->filter('a')->each(function (Crawler $node){
            if ($node->filter('i')->count() > 0) {
                return preg_replace('/<i>(.*?)<\/i>/s', "", $node->html());
            }
            return $node->text();
        });
    }
    private function getExtraEVInformation(Crawler $node)
    {
        //TODO EV DATA
        return null;
    }
}
