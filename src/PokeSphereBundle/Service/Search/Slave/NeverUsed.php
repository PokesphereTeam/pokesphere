<?php
namespace PokeSphereBundle\Service\Search\Slave;
use PokeSphereBundle\Entity\Stat;
use PokeSphereBundle\Service\Search\SearchSlave;
class NeverUsed extends SearchSlave
{
    private const SITE_NAME = "neverused";
    /**
     * Use SearchSlave to complete search strat file. Then merge and save its.
     *
     * @param $pokemon
     * @return Stat
     */
    public function search($pokemon): Stat
    {
        $this->stats->setSite(self::SITE_NAME);
        $this->stats->setUrl($this->baseUrl . $pokemon);
        $this->stats->setInfo($this->getInfo($pokemon));
        return $this->stats;
    }
    public function getInfo(string $pokemon)
    {
        // TODO: Implement getInfo() method.
    }
}
