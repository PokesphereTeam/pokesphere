<?php
namespace PokeSphereBundle\Service\Search;
use GraphAware\Neo4j\OGM\EntityManager;
use PokeSphereBundle\Entity\Stat;
use PokeSphereBundle\Service\Search\Slave\Pokestrat;
class SearchMaster implements SearchInterface
{
    private const SEVEN_DAYS_IN_SEC = 604800;
    /**
     * @var SearchSlave[]
     */
    private $slaves;
    /**
     * @var EntityManager
     */
    private $em;
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        // TODO use factory
        $this->slaves = [new Pokestrat('http://pokestrat.com/fiche-pokemon/')];
    }
    /**
     * Use SearchSlave to complete search strat file. Then merge and save its.
     * 
     * @param $pokemon
     * @return array
     */
    public function search($pokemon)
    {
        $statsCached = $this->em->getRepository(Stat::class)->searchStat($pokemon);
        if (!$this->isPokemonCached($statsCached)) {
            $stats = $this->makeWorkSlaves($pokemon);
        } else {
            $stats = $this->processStats($pokemon, $statsCached);
        }
        $this->save($stats);
        return $stats;
    }
    private function processStats($pokemon, $statsCached) {
        if (!$this->isTimestampValid($statsCached)) {
            $statsCached = $this->updatePokemonStat($pokemon, $statsCached);
        }
        return $statsCached;
    }
    private function updatePokemonStat($pokemon, $statsCached) {
        $stats = $this->makeWorkSlaves($pokemon);
        /** @var Stat $statCached */
        foreach ($statsCached as $statCached) {
            /** @var Stat $stat */
            foreach ($stats as $stat) {
                if ($statCached->getSite() === $stat->getSite()) {
                    if ($statCached->getInfo() !== $stat->getInfo()) {
                        $this->resetStat($statCached, $stat);
                    }
                    break;
                }
            }
        }
        return $statsCached;
    }
    private function resetStat(Stat $statCached, Stat $statTmp) {
        $statCached->setInfo($statTmp->getInfo());
        $statCached->setTimestamp($statTmp->getTimestamp());
        $statCached->setApprovers([]);
    }
    private function isPokemonCached($statsCached) {
        return count($statsCached) > 0;
    }
    private function isTimestampValid($statsCached)
    {
        /** @var Stat $stat */
        $validTimestamp = time() - self::SEVEN_DAYS_IN_SEC;
        foreach($statsCached as $stat) {
            if ($stat->getTimestamp() < $validTimestamp) {
                return false;
            }
        }
        return true;
    }
    private function makeWorkSlaves($pokemon)
    {
        $stats = [];
        foreach ($this->slaves as $slave) {
            $stats[] = $slave->search($pokemon);
        }
        return $stats;
    }
    /**
     * Save data on database.
     * @param array $stats
     */
    private function save(array $stats)
    {
        foreach ($stats as $stat) {
            $this->em->persist($stat);
        }
        $this->em->flush();
    }
}
