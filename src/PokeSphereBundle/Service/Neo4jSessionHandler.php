<?php
namespace PokeSphereBundle\Service;
use GraphAware\Neo4j\OGM\EntityManager;
use PokeSphereBundle\Entity\Session;
use PokeSphereBundle\Repository\SessionRepository;
class Neo4jSessionHandler implements \SessionHandlerInterface
{
    /** @var EntityManager $neo4j */
    private $neo4j;
    /** @var SessionRepository $sessionRepo */
    private $sessionRepo;
    public function __construct(EntityManager $neo4j)
    {
        $this->neo4j = $neo4j;
        $this->sessionRepo = $this->neo4j->getRepository(Session::class);
    }
    public function open($save_path, $name)
    {
        return true;
    }
    public function close()
    {
        return true;
    }
    public function gc($maxlifetime)
    {
        $this->neo4j->createQuery('
            MATCH (s:Session) 
            WHERE s.sess_lifetime + sess_time < {time}
            DELETE s'
        )->execute();
    }
    public function read($session_id)
    {
        $sessData = $this->neo4j->createQuery('
            MATCH (s:Session {sess_id: {sess_id}}) 
            WHERE (s.sess_time + s.sess_lifetime) >= (timestamp()/1000)
            RETURN s.sess_data'
        )
            ->setParameter('sess_id', $session_id)
            ->getOneOrNullResult();
        if ($sessData)
            return $sessData[0]['s.sess_data'];
        return '';
    }
    public function write($session_id, $session_data)
    {
        $this->neo4j->createQuery('
            MERGE (s:Session {sess_id: {sess_id}})
            SET    s.sess_data       = {sess_data},
                   s.sess_lifetime   = {sess_lifetime},
                   s.sess_time       = (timestamp()/1000)
        ')
            ->setParameter('sess_id',       $session_id)
            ->setParameter('sess_data',     $session_data)
            ->setParameter('sess_lifetime', (int) ini_get('session.gc_maxlifetime'))
            ->execute();
        return true;
    }
    public function destroy($session_id)
    {
        $this->neo4j->createQuery('MATCH (s:Session {sess_id: {sess_id}}) DELETE s')
            ->setParameter('sess_id', $session_id)
            ->execute();
        return true;
    }
}
