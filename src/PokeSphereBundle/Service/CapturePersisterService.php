<?php
namespace PokeSphereBundle\Service;
use GraphAware\Neo4j\OGM\EntityManager;
use GraphAware\Neo4j\OGM\Query;
use PokeSphereBundle\Entity\Ball;
use PokeSphereBundle\Entity\Capture;
use PokeSphereBundle\Entity\Game;
use PokeSphereBundle\Entity\IVInterval;
use PokeSphereBundle\Entity\Move;
use PokeSphereBundle\Entity\Nature;
use PokeSphereBundle\Entity\PokeItem;
use PokeSphereBundle\Entity\Pokemon;
use PokeSphereBundle\Entity\Talent;
use PokeSphereBundle\Listener\EntityEvent;
use PokeSphereBundle\Service\Voter\CaptureVoter;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
class CapturePersisterService
{
    private $neo4j;
    private $eventDispatcher;
    private $authChker;
    public function __construct(EntityManager $neo4j, EventDispatcher $eventDispatcher, AuthorizationCheckerInterface $authChker)
    {
        $this->neo4j = $neo4j;
        $this->eventDispatcher = $eventDispatcher;
        $this->authChker = $authChker;
    }
    /**
     * Warning: will empty neo4j's UnitOfWork to avoid a bug of the OGM
     */
    public function createFromData(\stdClass $data, ?Capture &$capture)
    {
        $moveRepo = $this->neo4j->getRepository(Move::class);
        /** @var Pokemon $pokemon */
        $pokemon = $this->neo4j->getRepository(Pokemon::class)->findOneById($data->pokemonId);
        /** @var Game $game */
        $game = $this->neo4j->getRepository(Game::class)->findOneById($data->gameId);
        /** @var Talent $talent */
        $talent = $this->neo4j->getRepository(Talent::class)->findOneById($data->talentId);
        /** @var Nature $nature */
        $nature = $this->neo4j->getRepository(Nature::class)->findOneById($data->natureId);
        /** @var Ball $ball */
        $ball = $this->neo4j->getRepository(Ball::class)->findOneById($data->ballId);
        /** @var PokeItem $pokeItem */
        $pokeItem = $this->neo4j->getRepository(PokeItem::class)->findOneById($data->objectId);
        if (! $capture)
        {
            $capture = new Capture();
            $capture->isNew = true;
        }
        if (! $this->authChker->isGranted(CaptureVoter::EDIT, $capture))
        {
            throw new AccessDeniedException();
        }
        $capture->setShiny($data->shiny);
        $capture->setNickname($data->nickname);
        $capture->setSex($data->sex);
        $capture->setPokemon($pokemon);
        $capture->setBall($ball);
        $capture->setGame($game);
        $capture->setTalent($talent);
        $capture->setNature($nature);
        $capture->setPokeItem($pokeItem);
        foreach ($data->moves as $move)
        {
            $capture->getMoves()->add($moveRepo->findOneById($move));
        }
        $evs = $data->ev;
        $capture->setHpEv    ($evs->hp);
        $capture->setAtkEv   ($evs->atk);
        $capture->setDefEv   ($evs->def);
        $capture->setSpeAtkEv($evs->speAtk);
        $capture->setSpeDefEv($evs->speDef);
        $capture->setSpeedEv ($evs->speed);
        $ivs = $data->iv;
        $capture->setHpIv    ($this->findIvInterval($ivs->hp));
        $capture->setAtkIv   ($this->findIvInterval($ivs->atk));
        $capture->setDefIv   ($this->findIvInterval($ivs->def));
        $capture->setSpeAtkIv($this->findIvInterval($ivs->speAtk));
        $capture->setSpeDefIv($this->findIvInterval($ivs->speDef));
        $capture->setSpeedIv ($this->findIvInterval($ivs->speed));
        $this->neo4j->clear();
    }
    public function findIvInterval(string $ivString) : IVInterval
    {
        $ivRepository = $this->neo4j->getRepository(IVInterval::class);
        $minMax = explode($ivString, '-');
        return $ivRepository->findOneBy(['min' => $minMax[0], 'max' => $minMax[1]]);
    }
    public function persist(Capture &$capture)
    {
        $entityEvent = new EntityEvent($capture);
        $this->eventDispatcher->dispatch(EntityEvent::PREFLUSH_SAVE_CAPTURE, $entityEvent);
        $this->eventDispatcher->dispatch(EntityEvent::ONFLUSH_SAVE_CAPTURE,  $entityEvent);
        if (isset($capture->isNew))
            $this->createCaptureNode($capture);
        $this->bindParams($this->getQueryForUpdate(), $capture)->getOneResult();
        $this->replaceRelation($capture, $capture->getGame(),     'Game',       'CAUGHT_IN');
        $this->replaceRelation($capture, $capture->getBall(),     'Ball',       'CAUGHT_WITH');
        $this->replaceRelation($capture, $capture->getPokemon(),  'Pokemon',    'INSTANCE_OF');
        $this->replaceRelation($capture, $capture->getTalent(),   'Talent',     'HAS_TALENT');
        $this->replaceRelation($capture, $capture->getNature(),   'Nature',     'HAS_NATURE');
        $this->replaceRelation($capture, $capture->getPokeItem(), 'PokeItem',   'HOLDS');
        $this->replaceRelation($capture, $capture->getHpIv(),     'IVInterval', 'HAS_HP_IV');
        $this->replaceRelation($capture, $capture->getAtkIv(),    'IVInterval', 'HAS_ATK_IV');
        $this->replaceRelation($capture, $capture->getDefIv(),    'IVInterval', 'HAS_DEF_IV');
        $this->replaceRelation($capture, $capture->getSpeAtkIv(), 'IVInterval', 'HAS_SPE_ATK_IV');
        $this->replaceRelation($capture, $capture->getSpeDefIv(), 'IVInterval', 'HAS_SPE_DEF_IV');
        $this->replaceRelation($capture, $capture->getSpeedIv(),  'IVInterval', 'HAS_SPEED_IV');
        $this->deleteRelations($capture, 'Move', 'HAS_MOVES');
        foreach ($capture->getMoves() as $move)
        {
            $this->addRelation($capture, $move, 'Move', 'HAS_MOVES');
        }
        $this->eventDispatcher->dispatch(EntityEvent::POSTFLUSH_SAVE_CAPTURE, $entityEvent);
    }
    #region node management
    protected function createCaptureNode(Capture &$capture)
    {
        $capture->setId(
            $this->neo4j->createQuery("CREATE (c:Capture) RETURN id(c)")->getOneResult()['id(c)']
        );
    }
    protected function getQueryForUpdate() : Query
    {
        return $this->neo4j->createQuery("
            MATCH (c:Capture) 
            WHERE id(c)={captureId} 
            SET
                c.nickname   = {nickname},
                c.shiny      = {shiny},
                c.sex        = {sex},
                c.hp_ev      = {hp_ev},
                c.atk_ev     = {atk_ev},
                c.def_ev     = {def_ev},
                c.spe_atk_ev = {spe_atk_ev},
                c.spe_def_ev = {spe_def_ev},
                c.speed_ev   = {speed_ev}
            RETURN c
        ");
    }
    protected function bindParams(Query $query, Capture $capture) : Query
    {
        return $query
            ->setParameter('captureId',  $capture->getId())
            ->setParameter('nickname',   $capture->getNickname())
            ->setParameter('shiny',      $capture->getShiny())
            ->setParameter('sex',        $capture->getSex())
            ->setParameter('hp_ev',      $capture->getHpEv())
            ->setParameter('atk_ev',     $capture->getAtkEv())
            ->setParameter('def_ev',     $capture->getDefEv())
            ->setParameter('spe_atk_ev', $capture->getSpeAtkEv())
            ->setParameter('spe_def_ev', $capture->getSpeDefEv())
            ->setParameter('speed_ev',   $capture->getSpeedEv())
            ->addEntityMapping('c',      Capture::class);
    }
    #endregion
    #region relation management
    /**
     * Deletes all relations named $relationName starting from the capture object toward any node of label $nodeLabel
     */
    protected function deleteRelations(Capture $capture, string $nodeLabel, string $relationName) : void
    {
        $this->neo4j->createQuery("
            MATCH  (c:Capture)-[r:$relationName]->(e:$nodeLabel) 
            WHERE  id(c)={captureId}
            DELETE r
        ")
            ->setParameter('captureId', $capture->getId())
            ->execute();
    }
    protected function addRelation(Capture $capture, $entity, string $nodeLabel, string $relationName) : void
    {
        if (! $entity)
            return;
        $this->neo4j->createQuery("
            MATCH  (c:Capture), (e:$nodeLabel) 
            WHERE  id(c)={captureId} AND id(e)={entityId}
            CREATE (c)-[r:$relationName]->(e)
        ")
            ->setParameter('captureId', $capture->getId())
            ->setParameter('entityId',  $entity->getId())
            ->execute();
    }
    protected function replaceRelation(Capture $capture, $entity, string $nodeLabel, string $relationName) : void
    {
        $this->deleteRelations($capture, $nodeLabel, $relationName);
        $this->addRelation($capture, $entity, $nodeLabel, $relationName);
    }
    #endregion
}
