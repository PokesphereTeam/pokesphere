<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 6/1/2017
 * Time: 11:45 AM
 */
namespace PokeSphereBundle\Service\ObjectToJson;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
class DataSerializer
{
    public final function serializeData($data,array $groups = null, int $circularLimit = 1){
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $normalizer = new ObjectNormalizer($classMetadataFactory);
        $normalizer->setCircularReferenceLimit($circularLimit);
        $normalizer->setCircularReferenceHandler(function($obj){
            return null;
        });
        $callback = function ($dateTime) {
            return $dateTime instanceof \DateTime
                ? $dateTime->getTimestamp()
                : '';
        };
        $normalizer->setCallbacks(['createdAt' => $callback, 'dateOfBirth' => $callback]);
        $encoder = new JsonEncoder();
        $serializer = new Serializer([$normalizer], [$encoder]);
        $opt = [ObjectNormalizer::ENABLE_MAX_DEPTH => true];
        if($groups)
            $opt[ObjectNormalizer::GROUPS] = $groups;
        $data = $serializer->normalize($data, null,$opt);
        return $data;
    }
}
