<?php
namespace PokeSphereBundle\Requester;
use GraphAware\Neo4j\OGM\EntityManager;
use PokeSphereBundle\Entity\ActivationRequest;
use PokeSphereBundle\Entity\EmailRequest;
use PokeSphereBundle\Entity\PasswordRequest;
use PokeSphereBundle\Entity\UpdateRequest;
use PokeSphereBundle\Entity\User;
class MailRequester
{
    private $mailer;
    private $translator;
    private $twig;
    /** @var EntityManager $em */
    private $em;
    public function __construct($mailer,$translator,$twig,$neo4j)
    {
        $this->mailer = $mailer;
        $this->translator = $translator;
        $this->twig = $twig;
        $this->em = $neo4j;
    }
    private function sendMailRequest(string $subject, string $view, UpdateRequest $urequest)
    {
        /** @var User $user */
        $user = $urequest->getUser();
        $email = \Swift_Message::newInstance()
            ->setSubject($this->translator->trans($subject))
            ->setFrom('noreply@pokesphere.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->twig->render($view, [
                    'user' => $user,
                    'request' => $urequest
                ]),
                'text/html'
            );
        $this->mailer->send($email);
    }
    public function sendEmailRequest(User $user, string $actual_email)
    {
        if ($user && $user->getIsValidated() && $user->getIsActive()) {
            $this->em->getRepository(EmailRequest::class)->setAllWaitingsToCanceled($user);
            $urequest = new EmailRequest();
            $urequest->setNewEmail($user->getEmail());
            $urequest->setUser($user);
            $this->em->persist($urequest);
            $this->em->flush();
            $this->sendMailRequest("registration",
                    "PokeSphereBundle:Mails:email_validation_mail.html.twig", $urequest);
            $user->setEmail($actual_email);
        }
    }
    public function sendPasswordRequest(User $user)
    {
        if ($user && $user->getIsValidated() && $user->getIsActive()) {
            $this->em->getRepository(PasswordRequest::class)->setAllWaitingsToCanceled($user);
            $urequest = new PasswordRequest();
            $urequest->setUser($user);
            $this->em->persist($urequest);
            $this->em->flush();
            $this->sendMailRequest("change password",
                    "PokeSphereBundle:Mails:change_password_mail.html.twig", $urequest);
        }
    }
    public function sendActivationRequest(User $user)
    {
        if ($user && !$user->getIsValidated() && $user->getIsActive()) {
            $this->em->getRepository(ActivationRequest::class)->setAllWaitingsToCanceled($user);
            $urequest = new ActivationRequest();
            $urequest->setUser($user);
            $this->em->persist($urequest);
            $this->em->flush();
            $this->sendMailRequest("registration",
                    "PokeSphereBundle:Mails:validation_mail.html.twig", $urequest);
        }
    }
}
