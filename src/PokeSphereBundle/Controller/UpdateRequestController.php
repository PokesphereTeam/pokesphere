<?php
namespace PokeSphereBundle\Controller;
use PokeSphereBundle\Entity\ActivationRequest;
use PokeSphereBundle\Entity\EmailRequest;
use PokeSphereBundle\Entity\EnumRequestStatut;
use PokeSphereBundle\Entity\PasswordRequest;
use PokeSphereBundle\Entity\UpdateRequest;
use PokeSphereBundle\Entity\User;
use PokeSphereBundle\Form\UpdateRequestPasswordUpdateType;
use PokeSphereBundle\Form\UpdateRequestType;
use PokeSphereBundle\Form\UpdateRequestValidationType;
use PokeSphereBundle\Repository\UpdateRequestRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/request")
 */
class UpdateRequestController extends AjaxController
{
    #region Activation
    /**
     * @Route("/activation/resend", name="resend_activation_request")
     * @Method("POST")
     */
    public function resendActivationRequestAction(Request $request)
    {
        $em = $this->get('neo4j');
        if(!$this->get('session')->has("new_user_id"))
            return $this->createErrorJsonResponse(null, Response::HTTP_UNAUTHORIZED);
        $form = $this->createForm(UpdateRequestType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $user = $em->getRepository(User::class)->findOneById($this->get('session')->get("new_user_id"));
            /** @var User $user */
            $user->setEmail($form->get('email')->getData());
            $this->get('mail_requester')->sendActivationRequest($user);
        return $this->json(null,Response::HTTP_NO_CONTENT);
        }
        return $this->createFormJsonErrorResponse($form);
    }
    /**
     * @Route("/activation", name="account_validate")
     * @Method("POST")
     */
    public function validateAccountAction(Request $request)
    {
        return $this->requestValidation($request, ActivationRequest::class,
            function ($w_request, User $user, $form)
            {
                $w_request->setStatut(EnumRequestStatut::VALIDATED);
                $user->setIsValidated(true);
                if($this->get('session')->has('new_user_id'))
                    $this->get('session')->remove('new_user_id');
                return $this->json(null,Response::HTTP_NO_CONTENT);
            }
        );
    }
    #endregion
    #region Password
    /**
     * @Route("/password", name="change_password_request")
     * @Method("POST")
     */
    public function sendPasswordRequestAction(Request $request)
    {
        $form = $this->createForm(UpdateRequestType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->get('neo4j');
            $user = $em->getRepository(User::class)->findOneBy(['email' => $form->get('email')->getData()]);
            if($user)
                $this->get('mail_requester')->sendPasswordRequest($user);
        return $this->json(null,Response::HTTP_NO_CONTENT);
        }
        return $this->createFormJsonErrorResponse($form);
    }
    /**
     * @Route("/password/update", name="update_password")
     * @Method("POST")
     */
    public function updatePasswordAction(Request $request)
    {
        return $this->requestValidation(
            $request,
            PasswordRequest::class,
            function ($w_request, User $user, $form)
            {
                $user->setPassword($form->getData()->getPassword());
                $w_request->setStatut(EnumRequestStatut::VALIDATED);
                return $this->json(null,Response::HTTP_NO_CONTENT);
            },
            UpdateRequestPasswordUpdateType::class,
            new User()
        );
    }
    #endregion
    #region Email
    /**
     * @Route("/email/validation", name="update_email")
     * @Method("POST")
     */
    public function validationEmailAction(Request $request)
    {
        return $this->requestValidation($request,EmailRequest::class,
            function ($w_request, User $user, Form $form)
            {
                $w_request->setStatut(EnumRequestStatut::VALIDATED);
                $user->setEmail($w_request->getNewEmail());
                return $this->json(null,Response::HTTP_NO_CONTENT);
            }
        );
    }
    #endregion
    private function requestValidation($request, $requestClazz, $call_exist,
                                       $formClazz = UpdateRequestValidationType::class, $formParams = null)
    {
        if($formParams)
            $form = $this->createForm($formClazz,$formParams);
        else
            $form = $this->createForm($formClazz);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            /** @var User $user */
            $user = $this->get('neo4j')->getRepository(User::class)
                ->findOneBy(['email'=>$form->get('email')->getData()]);
            if(!$user)
                return $this->json(null,Response::HTTP_BAD_REQUEST);
            $em = $this->get('neo4j');
            /** @var UpdateRequestRepository $request_repo */
            $request_repo = $em->getRepository($requestClazz);
            /** @var UpdateRequest $w_request */
            $w_request = $request_repo->getWaitingRequest($user, $form->get('token')->getData());
            if($w_request)
            {
                $current_date = new \DateTime('NOW');
                $createAt = new \DateTime();
                $createAt->setTimestamp($w_request->getCreatedAt());
                if((($current_date->getTimestamp() - $createAt->getTimestamp())/60/60) > UpdateRequest::EXPIRATION_HOUR_REQUEST) {
                    $w_request->setStatut(EnumRequestStatut::EXPIRED);
                    return $this->json(null,Response::HTTP_NOT_FOUND);
                }
                return $call_exist($w_request,$user,$form);
            }
            return $this->json(null,Response::HTTP_BAD_REQUEST);
        }
        return $this->createFormJsonErrorResponse($form);
    }
}
