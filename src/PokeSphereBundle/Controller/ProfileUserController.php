<?php
/**
 * Created by PhpStorm.
 * User: Sobraz
 * Date: 24/05/2017
 * Time: 23:21
 */
namespace PokeSphereBundle\Controller;
use PokeSphereBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
/**
 * @Route("/user")
 */
class ProfileUserController extends AjaxController
{
    /**
     * @Route("/{pseudo}", name="ws_get_user_profil")
     * @Method("GET")
     */
    public function getProfilAction($pseudo)
    {
        /** @var User $user */
        $user = $this->get('neo4j')->getRepository(User::class)->findOneBy(['pseudo' => $pseudo]);
        if(!$user)
            return $this->json([],Response::HTTP_NOT_FOUND);
        $groupsData = ["userPublicInfo","avatarInfo"];
        if($this->getUser() && ($user->getId() == $this->getUser()->getId()
            || $user->getSphereZonedUser($this->getUser()->getId())))
            $groupsData[] = "userPrivateInfo";
        $data = $this->serializeData($user,$groupsData);
        return $this->json($data);
    }
    /**
     * @Route("/{pseudo}/follows")
     * @Method("POST")
     */
    public function followedCommuAction($pseudo = null){
        /** @var User $user */
        $user = $this->get('neo4j')->getRepository(User::class)->findOneBy(['pseudo' => $pseudo]);
        $coms = $user->getFollowedCommunities();
        return $this->json($this->serializeData($coms,['commuInfo','avatarInfo']));
    }
}
