<?php
namespace PokeSphereBundle\Controller;
use Doctrine\Common\Annotations\AnnotationReader;
use PokeSphereBundle\Entity\Game;
use PokeSphereBundle\Entity\GameSerie;
use PokeSphereBundle\Entity\GameVersion;
use PokeSphereBundle\Entity\User;
use PokeSphereBundle\Form\GameType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
/**
 * @Route("/games")
 */
class GameController extends AjaxController
{
    /**
     * @Route("/{gameId}")
     * @Route("/")
     * @Method("POST")
     */
    public function putGameAction(Request $request, $gameId = null)
    {
        $em = $this->get('neo4j');
        /** @var Game $game */
        $game = $gameId ? $em->getRepository(Game::class)->findOneById($gameId) : null;
        // si l'id du jeu est donné mais qu'il n'est pas trouvé OU que le jeu n'est pas celui du joueur
        if(($gameId && !$game) || ($game && $game->getUser()->getId() != $this->getUser()->getId()))
            return $this->json(null,Response::HTTP_BAD_REQUEST);
        $form = $game ? $this->createForm(GameType::class,$game)
                        : $this->createForm(GameType::class,new Game());
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $game = $form->getData();
            /** @var User $user */
            $user = $em->getRepository(User::class)->findOneById($this->getUser()->getId());
            $game->setUser($user);
            $em->persist($game);
            $em->flush();
            if($game->getId() === null )
                return $this->json(null, Response::HTTP_CREATED);
            return $this->json(null,Response::HTTP_NO_CONTENT);
        }
        return $this->createErrorJsonResponse($this->get('form2json')->formErrorsToArray($form),Response::HTTP_BAD_REQUEST);
    }
    /**
     * @Route("/{gameId}")
     * @Method("DELETE")
     */
    public function deleteGameAction($gameId = null)
    {
        $em = $this->get('neo4j');
        /** @var Game $game */
        $game = $em->getRepository(Game::class)->findOneById($gameId);
        if(!$game || $game->getUser()->getId() != $this->getUser()->getId())
            return $this->json(null, Response::HTTP_BAD_REQUEST);
        $em->getRepository(Game::class)->detachAndDeleteGame($game);
        $em->flush();
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
    /**
     * @Route("/{id}")
     * @Method("GET")
     */
    public function getAllGamesAction($id)
    {
        $user_repo = $this->get('neo4j')->getRepository(User::class);
        /** @var User $user */
        $user = $user_repo->findOneById($id);
        if(!$user)
            return $this->json(null,Response::HTTP_BAD_REQUEST);
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $normalizer = new ObjectNormalizer($classMetadataFactory);
        $encoder = new JsonEncoder();
        $serializer = new Serializer([$normalizer], [$encoder]);
        $data = $serializer->normalize($user->getGames(), null, ['groups' => ["game_info","versionInfo"]]);
        return $this->json($data);
    }
    
}
