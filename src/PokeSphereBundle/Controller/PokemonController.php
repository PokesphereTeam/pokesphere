<?php
namespace PokeSphereBundle\Controller;
use PokeSphereBundle\Entity\Pokemon;
use PokeSphereBundle\Repository\PokemonRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
/**
 * @Route("/pokemon")
 */
class PokemonController extends AjaxController
{
    /**
     * @Route("/list", name="pokemon_get_list")
     * @Method("GET")
     */
    public function getListAction()
    {
        $neo4j = $this->get('neo4j');
        /** @var PokemonRepository $repo */
        $repo = $neo4j->getRepository(Pokemon::class);
        $pokemons = $repo->findAll();
        return $this->json($this->serializeData($pokemons, ['pokemon_basic_info']));
    }
    /**
     * @Route("/details/{pokemon_id}", name="pokemon_get_details")
     * @Method("GET")
     */
    public function getDetailsAction(int $pokemon_id)
    {
        $neo4j = $this->get('neo4j');
        $pokemon = $neo4j->getRepository(Pokemon::class)->findOneById($pokemon_id);
        return $this->json($this->serializeData($pokemon, ['pokemon_info']));
    }
}
