<?php
namespace PokeSphereBundle\Controller;
use PokeSphereBundle\Entity\Avatar;
use PokeSphereBundle\Entity\User;
use PokeSphereBundle\Form\ChangePasswordType;
use PokeSphereBundle\Form\DetailsType;
use PokeSphereBundle\Form\RegistrationType;
use PokeSphereBundle\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
class UserController extends AjaxController
{
    /**
     * @Route("/register")
     * @Method("POST")
     */
    public function registerAction(Request $request)
    {
        $form = $this->createForm(RegistrationType::class, new User());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->get('neo4j');
            /** @var User $user */
            $user = $form->getData();
            $default_a = $em->getRepository(Avatar::class)->findBy(['tag' => "default"]);
            foreach($default_a as $avatar) {
                if($avatar->getIsAvatarUser()){
                    $user->setAvatar($avatar);
                    break;
                }
            }
            $em->persist($user);
            $em->flush();
            $this->get('mail_requester')->sendActivationRequest($user);
            $this->get('session')->set("new_user_id", $user->getId());
            return $this->json($this->serializeData($user,['userPrivateInfo','userPublicInfo','details','avatarInfo']), Response::HTTP_CREATED);
        }
        return $this->createErrorJsonResponse($this->get('form2json')->formErrorsToArray($form));
    }
    /**
     * @Route("/details", name="ws_post_details")
     * @Method("POST")
     */
    public function updateDetailsAction(Request $request)
    {
        $em = $this->get("neo4j");
        /* @var User $user */
        $user = $em->getRepository(User::class)->findOneById($this->getUser()->getId());
        $current_email = $user->getEmail();
        $form = $this->createForm(DetailsType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if($current_email != $user->getEmail())
            {
                $this->get('mail_requester')->sendEmailRequest($user,$current_email);
                $user->setEmail($current_email);
            }
            $em->persist($user);
            $em->flush();
            return $this->json(null, Response::HTTP_NO_CONTENT);
        }
        return $this->json($this->get("form2json")->formErrorsToArray($form),Response::HTTP_BAD_REQUEST);
    }
    /**
     * @Route("/details", name="ws_get_details")
     * @Method("GET")
     */
    public function getDetailsAction()
    {
        $user_id = $this->getUser()->getId();
        $user = $this->get('neo4j')->getRepository(User::class)->findOneById($user_id);
        return $this->json($this->serializeData($user,["details"]));
    }
    /**
     * @Route("/details/password")
     * @Method("POST")
     */
    public function changePasswordAction(Request $request){
        $form = $this->createForm(ChangePasswordType::class,$this->getUser());
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->get('neo4j');
            $user = $em->getRepository(User::class)->findOneById($this->getUser()->getId());
            $user->setPassword($this->getUser()->getPassword());
            return $this->json(null, Response::HTTP_NO_CONTENT);
        }
        return $this->createFormJsonErrorResponse($form);
    }
    /**
     * @Route("/user/pseudo_exists/{pseudo}", name="ws_user_pseudo_exists")
     * @Method("GET")
     */
    public function getUserPseudoExistsAction($pseudo)
    {
        /** @var UserRepository $repo */
        $repo = $this->get('neo4j')->getRepository(User::class);
        $user = $repo->pseudoCaseInsensitiveExist($pseudo);
        return new JsonResponse(!($user == null || empty($user)));
    }
}
