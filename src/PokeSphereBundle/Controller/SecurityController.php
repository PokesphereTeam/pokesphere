<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 5/18/2017
 * Time: 4:20 PM
 */
namespace PokeSphereBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
class SecurityController extends AjaxController
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request){
        return $this->json(null, Response::HTTP_BAD_REQUEST);
    }
}
