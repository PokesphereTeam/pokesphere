<?php
namespace PokeSphereBundle\Controller;
use Doctrine\Common\Annotations\AnnotationReader;
use PokeSphereBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
/**
 * @Route("/spherezone")
 */
class SphereZoneController extends AjaxController
{
    /**
     * @Route("/{userId}", name="ws_spherezone_add")
     * @Method("POST")
     */
    public function addToSphereZoneAction($userId)
    {
        if($userId == $this->getUser()->getId())
            return $this->json([],Response::HTTP_BAD_REQUEST);
        $em = $this->get("neo4j");
        /** @var User $user */
        $user = $em->getRepository(User::class)->findOneById($userId);
        $current_user = $em->getRepository(User::class)->findOneById($this->getUser()->getId());
        if(!$user)
            return $this->json([],Response::HTTP_BAD_REQUEST);
        $user->getSphereZoneRequesters()->add($current_user);
        $em->persist($user);
        $em->flush();
        return $this->json([],Response::HTTP_NO_CONTENT);
    }
    /**
     * @Route("/{userId}", name="ws_spherezone_remove")
     * @Method("DELETE")
     */
    public function removeFromSphereZoneAction($userId)
    {
        $em = $this->get("neo4j");
        /** @var User $user */
        $user = $em->getRepository(User::class)->findOneById($this->getUser()->getId());
        if($rem_user = $user->getSphereZoneRequester($userId))
        {
            $user->getSphereZoneRequesters()->removeElement($rem_user);
            $rem_user->getSphereZoneRequested()->removeElement($user);
        }
        else if($rem_user = $user->getSphereZoneUserRequested($userId))
        {
            $user->getSphereZoneRequested()->removeElement($rem_user);
            $rem_user->getSphereZoneRequesters()->removeElement($user);
        }
        else if ($rem_user = $user->getSphereZonedUser($userId))
        {
            $user->getSphereZone()->removeElement($rem_user);
            $rem_user->getSphereZone()->removeElement($user);
        }
        $em->flush();
        return $this->json([],Response::HTTP_NO_CONTENT);
    }
    /**
     * @Route("/accept/{userId}", name="ws_spherezone_accept")
     * @Method("POST")
     */
    public function acceptInSphereZoneAction($userId)
    {
        $em = $this->get("neo4j");
        /** @var User $user */
        $user = $em->getRepository(User::class)->findOneById($this->getUser()->getId());
        $new_friend = $user->getSphereZoneRequester($userId);
        if(!$new_friend)
            return $this->json([],Response::HTTP_BAD_REQUEST);
        $user->getSphereZoneRequesters()->removeElement($new_friend);
        $new_friend->getSphereZoneRequested()->removeElement($user);
        $user->getSphereZone()->add($new_friend);
        $em->flush();
        return $this->json([],Response::HTTP_NO_CONTENT);
    }
    /**
     * @Route("/requesters")
     * @Method("GET")
     */
    public function getRequesterAction()
    {
        /** @var User $user */
        $user = $this->get("neo4j")->getRepository(User::class)->findOneById($this->getUser()->getId());
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $normalizer = new ObjectNormalizer($classMetadataFactory);
        $encoder = new JsonEncoder();
        $serializer = new Serializer([$normalizer], [$encoder]);
        $data = $serializer->normalize($user->getSphereZoneRequesters(), null, ['groups' => ["userPublicInfo"]]);
        return $this->json($data);
    }
    /**
     * @Route("/requested")
     * @Method("GET")
     */
    public function getRequestedAction()
    {
        /** @var User $user */
        $user = $this->get("neo4j")->getRepository(User::class)->findOneById($this->getUser()->getId());
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $normalizer = new ObjectNormalizer($classMetadataFactory);
        $encoder = new JsonEncoder();
        $serializer = new Serializer([$normalizer], [$encoder]);
        $data = $serializer->normalize($user->getSphereZoneRequested(), null, ['groups' => ["userPublicInfo"]]);
        return $this->json($data);
    }
    /**
     * @Route("/")
     * @Route("/user/{idUser}")
     * @Method("GET")
     */
    public function getSphereZoneAction($idUser = null)
    {
        /** @var User $user */
        $user = $this->get("neo4j")->getRepository(User::class)
                    ->findOneById($idUser?$idUser:$this->getUser()->getId());
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $normalizer = new ObjectNormalizer($classMetadataFactory);
        $encoder = new JsonEncoder();
        $serializer = new Serializer([$normalizer], [$encoder]);
        $data = $serializer->normalize($user->getSphereZone(), null, ['groups' => ["userPublicInfo"]]);
        return $this->json($data);
    }
}
