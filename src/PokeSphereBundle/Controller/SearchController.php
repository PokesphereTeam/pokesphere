<?php
/**
 * Created by PhpStorm.
 * User: Sobraz
 * Date: 20/05/2017
 * Time: 00:10
 */
namespace PokeSphereBundle\Controller;
use PokeSphereBundle\Entity\Profile;
use PokeSphereBundle\Repository\ProfileRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
/**
 * @Route("/search")
 */
class SearchController extends AjaxController
{
    /**
     * @Route("/profile/{limit}/{skip}")
     * @Method("GET")
     */
    public function searchAction(Request $request,$limit,$skip)
    {
        $terms = $request->headers->get('search');
        /** @var ProfileRepository $p_repo */
        $p_repo = $this->get('neo4j')->getRepository(Profile::class);
        //$profils = ($this->getUser()) ?  $p_repo->searchProfileFromUser($this->getUser()->getId(),$terms,(int)$limit,(int)$skip)
        //                                    : $p_repo->searchProfileFromGuest($terms,(int)$limit,(int)$skip);
        $profils = $p_repo->searchProfileFromGuest($terms,(int)$limit,(int)$skip);
        return $this->json($this->serializeData($profils,['userPublicInfo','commuInfo','avatarInfo']));
    }
}
