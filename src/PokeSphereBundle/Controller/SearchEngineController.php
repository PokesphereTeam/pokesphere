<?php
namespace PokeSphereBundle\Controller;
use PokeSphereBundle\Entity\Stat;
use PokeSphereBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
/**
 * Class SearchEngineController
 * @package PokeSphereBundle\Controller
 * @Route("/strategy", name="search-engine")
 */
class SearchEngineController extends AjaxController
{
    /**
     * @param $pokemon
     * @Route("/search/{pokemon}", name="search-strategy-file")
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function searchAction($pokemon)
    {
        $searchEngine = $this->get('pokesphere.search.engine');
        $data = $searchEngine->search($pokemon);
        return $this->json($this->serializeData($data, ["stat_info","userPublicInfo"]), Response::HTTP_OK);
    }
    /**
     * @Route("/approve/{stratID}", name="approve-stat")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function approveStatAction($stratID)
    {
        $em = $this->get("neo4j");
        /** @var Stat $strat */
        $strat = $em->getRepository(Stat::class)->findOneById($stratID);
        $user = $em->getRepository(User::class)->findOneById($this->getUser()->getId());
        if (!in_array($strat, $user->getApprovedStats()->toArray())) {
            $strat->getApprovers()->add($user);
            $user->getApprovedStats()->add($strat);
            $em->persist($strat);
            $em->persist($user);
            return $this->json([], 200);
        } else {
            return $this->json(['error' => "Already Approved"], Response::HTTP_BAD_REQUEST);
        }
    }
}

