<?php
namespace PokeSphereBundle\Controller;
use JsonSchema\Validator;
use PokeSphereBundle\Entity\Capture;
use PokeSphereBundle\Repository\CaptureRepository;
use PokeSphereBundle\Service\Voter\CaptureVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
/**
 * @Route("/bank")
 */
class BankController extends AjaxController
{
    /**
     * @Route("/{userId}",                name="bank_get_captures", requirements={"userId": "\d+"})
     * @Route("/{userId}/{limit}",        name="bank_get_captures", requirements={"userId": "\d+", "limit": "\d+"})
     * @Route("/{userId}/{limit}/{skip}", name="bank_get_captures", requirements={"userId": "\d+", "limit": "\d+", "skip": "\d+"})
     * @Method("GET")
     */
    public function getCapturesAction(int $userId, int $limit=10, int $skip=0)
    {
        if ($limit > 100)
            throw new BadRequestHttpException("limit cannot exceed 100");
        /** @var CaptureRepository $captureRepo */
        $captureRepo = $this->get('neo4j')->getRepository(Capture::class);
        $captures = $captureRepo->findByUserId($userId, $limit, $skip);
        return $this->json($this->serializeData($captures, ['capture_info']));
    }
    /**
     * @Route("/create", name="bank_create_capture")
     * @Method("POST")
     */
    public function createCaptureAction(Request $request)
    {
        $jsonString = $request->getContent();
        $data = $this->validateJsonPayload($jsonString);
        $capturePersister = $this->get('pokesphere.capture.persister');
        /** @var Capture $capture */
        $capturePersister->createFromData($data, $capture);
        $capturePersister->persist($capture);
        return new JsonResponse($capture->getId(), Response::HTTP_CREATED);
    }
    /**
     * @Route("/update/{captureId}", name="bank_update_capture", requirements={"captureId": "\d+"})
     * @Method("POST")
     */
    public function updateCaptureAction(Request $request, int $captureId)
    {
        $jsonString = $request->getContent();
        $data = $this->validateJsonPayload($jsonString);
        /** @var Capture $capture */
        $capture = $this->get('neo4j')->getRepository(Capture::class)->findOneById($captureId);
        $capturePersister = $this->get('pokesphere.capture.persister');
        $capturePersister->createFromData($data, $capture);
        $capturePersister->persist($capture);
        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
    /**
     * @Route("/delete/{captureId}", name="bank_delete_capture", requirements={"captureId": "\d+"})
     * @Method("DELETE")
     */
    public function deleteCaptureAction(Request $request, int $captureId)
    {
        $neo4j = $this->get('neo4j');
        /** @var Capture $capture */
        $capture = $neo4j->getRepository(Capture::class)->findOneById($captureId);
        if (! $this->isGranted(CaptureVoter::DELETE, $capture))
            throw new AccessDeniedException();
        $neo4j->remove($capture, true);
        $neo4j->flush();
        return new JsonResponse();
    }
    private function validateJsonPayload(string &$jsonString) : \stdClass
    {
        $json = json_decode($jsonString);
        $validator = new Validator;
        $validator->validate($json, (object)[
            '$ref' => 'file://'.$this->get('kernel')->locateResource('@PokeSphereBundle/Resources/jsonschema/bank_post.json')
        ]);
        if (! $validator->isValid())
            throw new BadRequestHttpException("Schema violation");
        return $json;
    }
}
