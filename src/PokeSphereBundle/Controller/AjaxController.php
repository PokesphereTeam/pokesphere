<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 4/16/2017
 * Time: 11:52 PM
 */
namespace PokeSphereBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
abstract class AjaxController extends Controller
{
    private const JSON_PARAMS_INFOS = "infos";
    private const JSON_PARAMS_ERRORS = "errors";
    protected final function createJsonResponse(array $infos = [], array $errors = [], array $other = null,
                                                ?int $status = null, array $header = [], array $context = [])
    {
        $data = [];
        if(!empty($infos))
            $data[self::JSON_PARAMS_INFOS] = $infos;
        if(!empty($errors))
            $data[self::JSON_PARAMS_ERRORS] = $errors;
        if(!empty($other))
            $data = array_merge($data,$other);
        $status = $status ? $status : (isset($data[self::JSON_PARAMS_ERRORS]) ? Response::HTTP_BAD_REQUEST : Response::HTTP_OK);
        return $this->json($data,$status, $header, $context);
    }
    protected  final function createInfoJsonResponse(array $infos, ?int $status = Response::HTTP_OK)
    {
        return $this->createJsonResponse($infos,[],null,$status);
    }
    protected  final function createErrorJsonResponse(array $errors, ?int $status = Response::HTTP_BAD_REQUEST)
    {
        return $this->createJsonResponse([],$errors,null,$status);
    }
    protected final function serializeData($data,array $groups = null, int $circularLimit = 1){
        return $this->get('data_serializer')->serializeData($data, $groups, $circularLimit);
    }
    protected final function createFormJsonErrorResponse(Form $form){
        $errors = $this->get('form2json')->formErrorsToArray($form);
        return $this->createErrorJsonResponse($errors);
    }
}
