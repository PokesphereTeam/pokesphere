<?php
namespace PokeSphereBundle\Controller;
use PokeSphereBundle\Entity\Community;
use PokeSphereBundle\Entity\User;
use PokeSphereBundle\Form\CommunityType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
/**
 * @Route("/community")
 */
class CommunityController extends AjaxController
{
    /**
     * @Route("/")
     * @Route("/{cname}")
     * @Method("POST")
     */
    public function putCommunityAction(Request $request, $cname = null){
        $em = $this->get('neo4j');
        $com = $cname ? $em->getRepository(Community::class)->findOneBy(['name'=>$cname]) : null;
        /** @var Community $com */
        if($cname){
            if(!$com)
                return $this->json(null, Response::HTTP_NOT_FOUND);
            if($com->getOwner()->getId() != $this->getUser()->getId())
                return $this->json(null,Response::HTTP_FORBIDDEN);
        }
        $form = $cname ? $this->createForm(CommunityType::class,$com)
            : $this->createForm(CommunityType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $com = $form->getData();
            if(!$cname){
                $user = $em->getRepository(User::class)->findOneById($this->getUser()->getId());
                $com->setOwner($user);
                $com->setStaff([$user]);
            }
            $em->persist($com);
            $em->flush();
            return $this->json(null, Response::HTTP_NO_CONTENT);
        }
        return $this->createErrorJsonResponse($this->get('form2json')->formErrorsToArray($form));
    }
    /**
     * @Route("/{cname}")
     * @Method("DELETE")
     */
    public function deleteCommunityAction($cname){
        $em = $this->get("neo4j");
        /** @var Community $com */
        $com = $em->getRepository(Community::class)->findOneBy(['name'=>$cname]);
        if(!$com)
            return $this->json(null, Response::HTTP_NOT_FOUND);
        if($com && $com->getOwner()->getId() == $this->getUser()->getId()){
            $em->remove($com,true);
            $em->flush();
            return $this->json(null, Response::HTTP_NO_CONTENT);
        }
        return $this->json(null, Response::HTTP_FORBIDDEN);
    }
    /**
     * @Route("/{cname}")
     * @Method("GET")
     */
    public function getCommunityAction($cname){
        $em = $this->get('neo4j');
        $com = $em->getRepository(Community::class)->findOneBy(['name'=>$cname]);
        /** @var Community $com */
        if(!$com)
            return $this->json(null, Response::HTTP_NOT_FOUND);
        return $this->json($this->serializeData($com,['commuInfo','userPublicInfo','avatarInfo']));
    }
    /**
     * @Route("/{cname}/staff")
     * @Method("GET")
     */
    public function getStaffAction($cname){
        $em = $this->get('neo4j');
        /** @var Community $com */
        $com = $em->getRepository(Community::class)->findOneBy(['name'=>$cname]);
        if(!$com)
            return $this->json(null,Response::HTTP_NOT_FOUND);
        return $this->json($this->serializeData($com->getStaff(),['userPublicInfo','avatarInfo']));
    }
    /**
     * @Route("/{cname}/staff/{userid}")
     * @Method("POST")
     */
    public function addStaffAction($cname, $userid){
        $em = $this->get('neo4j');
        /** @var Community $com */
        $com = $em->getRepository(Community::class)->findOneBy(['name'=>$cname]);
        if(!$com)
            return $this->json(null,Response::HTTP_NOT_FOUND);
        if($com->getOwner()->getId() != $this->getUser()->getId())
            return $this->json(null,Response::HTTP_FORBIDDEN);
        if($com->getOwner()->getId() == $userid || $com->hasStaff($userid)){
            return $this->json(null,Response::HTTP_BAD_REQUEST);
        }
        $user = $em->getRepository(User::class)->findOneById($userid);
        if(!$user)
            return $this->json(null,Response::HTTP_BAD_REQUEST);
        $com->getStaff()->add($user);
        $em->flush();
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
    /**
     * @Route("/{cname}/staff/{userid}")
     * @Method("DELETE")
     */
    public function removeStaffAction(Request $request, $cname, $userid){
        $em = $this->get('neo4j');
        /** @var Community $com */
        $com = $em->getRepository(Community::class)->findOneBy(['name'=>$cname]);
        // si (la commu n'existe pas ou que l'utilisateur à supprimer est le propriétaire)
        // ou (que l'utilisateur n'est ni propriétaire ni la personne qu'il veut supprimer)
        // ou que le membre à supprimé n'est pas dans le staff
        if((!$com || $userid == $com->getOwner()->getId())
            || !($com->getOwner()->getId() == $this->getUser()->getId()
                || $userid == $this->getUser()->getId())
            || !($staff_user = $com->getStaffUser($userid)))
            return $this->json(null,Response::HTTP_BAD_REQUEST);
        $com->getStaff()->removeElement($staff_user);
        $em->flush();
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
    /**
     * @Route("/{cname}/owner/{userid}")
     * @Method("POST")
     */
    public function changeOwnerAction(Request $request, $cname, $userid){
        $em = $this->get('neo4j');
        /** @var Community $com */
        $com = $em->getRepository(Community::class)->findOneBy(['name'=>$cname]);
        if(!$com)
            return $this->json(null,Response::HTTP_NOT_FOUND);
        if($com->getOwner()->getId() != $this->getUser()->getId())
            return $this->json(null,Response::HTTP_FORBIDDEN);
        $staff_user = $com->getStaffUser($userid);
        if(!$staff_user)
            return $this->json(null,Response::HTTP_BAD_REQUEST);
        $com->setOwner($staff_user);
        $em->flush();
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
    /**
     * @Route("/{cname}/follow")
     * @Method("POST")
     */
    public function followAction($cname){
        $em = $this->get('neo4j');
        /** @var Community $com */
        $com = $em->getRepository(Community::class)->findOneBy(['name'=>$cname]);
        if(!$com)
            return $this->json(null,Response::HTTP_NOT_FOUND);
        /** @var User $user */
        $user = $em->getRepository(User::class)->findOneById($this->getUser()->getId());
        if($user->hasFollow($com->getId()))
            return $this->json(null,Response::HTTP_BAD_REQUEST);
        $user->getFollowedCommunities()->add($com);
        $em->flush();
        return $this->json(null,Response::HTTP_NO_CONTENT);
    }
    /**
     * @Route("/{cname}/follow")
     * @Method("DELETE")
     */
    public function unfollowAction($cname){
        $em = $this->get('neo4j');
        /** @var User $user */
        $user = $em->getRepository(User::class)->findOneById($this->getUser()->getId());
        if(!($com = $user->hasFollowByName($cname)))
            return $this->json(null,Response::HTTP_BAD_REQUEST);
        $user->getFollowedCommunities()->removeElement($com);
        $em->flush();
        return $this->json(null,Response::HTTP_NO_CONTENT);
    }
}
