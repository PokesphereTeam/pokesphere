<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 5/13/2017
 * Time: 1:21 PM
 */
namespace PokeSphereBundle\Controller;
use GraphAware\Neo4j\OGM\Events;
use PokeSphereBundle\Entity\Community;
use PokeSphereBundle\Entity\Post;
use PokeSphereBundle\Entity\PostBackup;
use PokeSphereBundle\Entity\User;
use PokeSphereBundle\Form\PostType;
use PokeSphereBundle\Listener\PrePersistPost;
use PokeSphereBundle\Listener\PrePersistPostBackup;
use PokeSphereBundle\Repository\PostRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
/**
 * @Route("/post")
 */
class PostController extends AjaxController
{
    /**
     * @Route("")
     * @Route("/community/{idCom}")
     * @Method("POST")
     */
    public function createPostAction(Request $request, $idCom = null)
    {
        $form = $this->createForm(PostType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->get('neo4j');
            /** @var Post $post */
            $post = $form->getData();
            /** @var User $writer */
            $writer = $em->getRepository(User::class)->findOneById($this->getUser()->getId());
            $post->setWriter($writer);
            if ($idCom)
            {
                /** @var Community $com */
                $com = $em->getRepository(Community::class)->findOneById($idCom);
                if (!$com)
                    return $this->json(null, Response::HTTP_NOT_FOUND);
                if (!$com->hasStaff($writer->getId()))
                    return $this->json(null, Response::HTTP_BAD_REQUEST);
            }
            $em->persist($post);
            $em->flush();
            $em->getRepository(Post::class)->sharePost(($idCom ? $idCom : $writer->getId()), $post->getId());
            return $this->json($this->serializeData($post, ['postInfo', 'userPublicInfo']), Response::HTTP_CREATED);
        }
        return $this->createFormJsonErrorResponse($form);
    }
    /**
     * @Route("/{idPost}")
     * @Method("POST")
     */
    public function updatePostAction(Request $request, $idPost)
    {
        $em = $this->get('neo4j');
        /** @var Post $post */
        $post = $em->getRepository(Post::class)->findOneById($idPost);
        if (!$post)
            return $this->json(null, Response::HTTP_NOT_FOUND);
        if ($post->getWriter()->getId() != $this->getUser()->getId())
            return $this->json(null, Response::HTTP_FORBIDDEN);
        $postBackup = PostBackup::createFromPost($post);
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $em->persist($postBackup);
            $em->flush();
            return $this->json(null, Response::HTTP_NO_CONTENT);
        }
        return $this->createFormJsonErrorResponse($form);
    }
    /**
     * @Route("/{idpost}")
     * @Method("DELETE")
     */
    public function deletePostAction($idpost)
    {
        $em = $this->get('neo4j');
        /** @var Post $post */
        $post = $em->getRepository(Post::class)->findOneById($idpost);
        if (!$post)
            return $this->json(null, Response::HTTP_NOT_FOUND);
        if ($post->getWriter()->getId() != $this->getUser()->getId())
            return $this->json(null, Response::HTTP_FORBIDDEN);
        /** @var PostRepository $post_repo */
        $post_repo = $em->getRepository(Post::class);
        $post_repo->clearAllShares($post);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
    /**
     * @Route("/share/{idPost}")
     * @Route("/community/{idCom}/share/{idPost}")
     * @Method("POST")
     */
    public function sharePostAction($idPost, $idCom = null)
    {
        $em = $this->get('neo4j');
        /** @var PostRepository $post_repo */
        $post_repo = $em->getRepository(Post::class);
        /** @var Post $post */
        $post = $post_repo->findOneById($idPost);
        if (!$post)
            return $this->json(null, Response::HTTP_NOT_FOUND);
        /** @var User $user */
        $user = $em->getRepository(User::class)->findOneById($this->getUser()->getId());
        if (!$post->getPublic())
            return $this->json(null, Response::HTTP_FORBIDDEN);
        if ($idCom)
        {
            /** @var Community $com */
            $com = $em->getRepository(Community::class)->findOneById($idCom);
            if (!$com)
                return $this->json(null, Response::HTTP_NOT_FOUND);
            if (!$com->hasStaff($post->getWriter()->getId()))
                return $this->json(null, Response::HTTP_BAD_REQUEST);
        }
        if ($post_repo->hasSharedPost($idCom ? $idCom : $user->getId(), $post->getId()))
            return $this->json(null, Response::HTTP_BAD_REQUEST);
        $post_repo->sharePost($idCom ? $idCom : $user->getId(), $post->getId());
        $em->flush();
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
    /**
     * @Route("/share/{idPost}")
     * @Route("/community/{idCom}/share/{idPost}")
     * @Method("DELETE")
     */
    public function unsharePostAction($idPost, $idCom = null)
    {
        $em = $this->get('neo4j');
        /** @var User $user */
        $user = $em->getRepository(User::class)->findOneById($this->getUser()->getId());
        /** @var PostRepository $post_repo */
        $post_repo = $em->getRepository(Post::class);
        if (!($post = $post_repo->hasSharedPost($user->getId(), $idPost)))
            return $this->json(null, Response::HTTP_BAD_REQUEST);
        $post = $post[0];
        if (!$idCom && $post->getWriter()->getId() == $this->getUser()->getId())
            $post_repo->clearAllShares($post);
        else
        {
            if ($idCom)
            {
                /** @var Community $com */
                $com = $em->getRepository(Community::class)->findOneById($idCom);
                if (!$com)
                    return $this->json(null, Response::HTTP_NOT_FOUND);
                if (!$com->hasStaff($post->getWriter()->getId()))
                    return $this->json(null, Response::HTTP_BAD_REQUEST);
            }
            $post_repo->unsharePost($idCom ? $idCom : $user->getId(), $post->getId());
        }
        $em->flush();
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
    /**
     * @Route("/user/{idUser}/shares/{page}/{nb}")
     * @Route("/community/{idCom}/shares/{page}/{nb}")
     * @Method("GET")
     */
    public function getSharedPostAction(?int $idUser = null, ?int $idCom = null, int $page = 0, int $nb = 15)
    {
        $em = $this->get('neo4j');
        /** @var PostRepository $post_repo */
        $post_repo = $em->getRepository(Post::class);
        if ($idUser !== null)
        {
            /** @var User $user */
            $user = $em->getRepository(User::class)->findOneById($idUser);
            if (!$user)
                return $this->json(null, Response::HTTP_BAD_REQUEST);
            if ($user->getId() == $this->getUser()->getId()
                || $user->getSphereZonedUser($this->getUser()->getId())
            )
                $posts = $post_repo->getSharedPosts($user->getId(), $page * $nb, $nb);
            else
                $posts = $post_repo->getPublicSharedPosts($user->getId(), $page * $nb, $nb);
        }
        else
        {
            $posts = $post_repo->getPublicSharedPosts((int)$idCom, $page * $nb, $nb);
        }
        return $this->json($this->serializeData($posts, ['postInfo', 'userPublicInfo']), Response::HTTP_OK);
    }
    public function reportPostAction($idPost)
    {
        //TODO
    }
}
