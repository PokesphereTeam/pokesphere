<?php
namespace PokeSphereBundle\Controller;
use PokeSphereBundle\Entity\GameSerie;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/series")
 */
class SeriesController extends AjaxController
{
    /**
     * @Route("")
     * @Method("GET")
     */
    public function getAllGameSeriesAction(){
        $gs = $this->get('neo4j')->getRepository(GameSerie::class)->findAll();
        return $this->json($this->serializeData($gs,['serieInfo','versionInfo','genInfo']));
    }
}
