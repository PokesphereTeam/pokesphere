<?php
namespace PokeSphereBundle\Controller;
use PokeSphereBundle\Entity\Nature;
use PokeSphereBundle\Repository\PokemonRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
/**
 * @Route("/nature")
 */
class NatureController extends AjaxController
{
    /**
     * @Route("/list", name="nature_get_list")
     * @Method("GET")
     */
    public function getListAction()
    {
        /** @var PokemonRepository $repo */
        $repo = $this->get('neo4j')->getRepository(Nature::class);
        $natures = $repo->findAll();
        return $this->json($this->serializeData($natures, ['nature_basic_info']));
    }
}
