<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 5/14/2017
 * Time: 11:44 PM
 */
namespace PokeSphereBundle\Controller;
use PokeSphereBundle\Entity\Avatar;
use PokeSphereBundle\Entity\Community;
use PokeSphereBundle\Entity\User;
use PokeSphereBundle\Repository\AvatarRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
/**
 * @Route("avatars")
 */
class AvatarController extends AjaxController
{
    /**
     * @Route("/communities")
     * @Route("/community/{idCom}")
     * @Method("GET")
     */
    public function getAvailableAvatarCommuAction($idCom = null){
        $em = $this->get('neo4j');
        /** @var Community $com */
        $com = $idCom ? $em->getRepository(Community::class)->findOneById($idCom) : null;
        if($idCom && !$com)
            return $this->json(null, Response::HTTP_NOT_FOUND);
        if($com && $com->getOwner()->getId() != $this->getUser()->getId())
            return $this->json(null, Response::HTTP_FORBIDDEN);
        /** @var AvatarRepository $repo_avatars */
        $repo_avatars = $em->getRepository(Avatar::class);
        $avatars = $repo_avatars->getAvalaibleCommunityAvatars($com);
        return $this->json($this->serializeData($avatars,['avatarInfo']));
    }
    /**
     * @Route("/user")
     * @Method("GET")
     */
    public function getAvailableAvatarUserAction(){
        /** @var AvatarRepository $repo_avatar */
        $repo_avatar = $this->get('neo4j')->getRepository(Avatar::class);
        $avatars = $repo_avatar->getAvalaibleUserAvatars($this->getUser()->getId());
        $data = $this->serializeData($avatars, ['avatarInfo']);
        return $this->json($data);
    }
}
