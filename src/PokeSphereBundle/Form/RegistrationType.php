<?php
namespace PokeSphereBundle\Form;
use GraphAware\Neo4j\OGM\EntityManager;
use PokeSphereBundle\Entity\SecurityUser;
use PokeSphereBundle\Entity\User;
use PokeSphereBundle\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
class RegistrationType extends AbstractType
{
    /** @var EncoderFactory $encoder */
    private $encoder;
    /** @var UserRepository $user_repo */
    private $user_repo;
    public function __construct(EncoderFactory $encoder, EntityManager $em)
    {
        $this->encoder = $encoder->getEncoder(new User());
        $this->user_repo = $em->getRepository(User::class);
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $encoder = $this->encoder;
        $current_year = date('Y');
        $builder
            ->add('pseudo')
            ->add('email', EmailType::class)
            ->add('dateOfBirth', BirthdayType::class, [
                'format' => 'dd/MM/yyyy',
                'years' => range($current_year - 10, $current_year - 80)
            ])
            ->add('friend_code', FriendCodeType::class, [
                'required' => false
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'required' => true,
                'first_options' => array('label' => 'Mot de passe'),
                'second_options' => array('label' => 'Confirmation')
            ])
            ->add('friendCode', FriendCodeType::class, ["required" => false])
            ->add('isAMan', ChoiceType::class, [
                'choices' => array(
                    'Yes' => true,
                    'No' => false,
                ),
                'expanded' => true,
                'multiple' => false,
                "required" => false,
                "empty_data" => null
            ])
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                $pseudo = $event->getForm()->get('pseudo')->getData();
                $db_user = $this->user_repo->pseudoCaseInsensitiveExist($pseudo);
                if (!empty($db_user))
                    $event->getForm()->get('pseudo')
                        ->addError(new FormError("user.pseudo.unique"));
                $email = $event->getForm()->get('email')->getData();
                $db_user = $this->user_repo->findBy(['email' => $email]);
                if (!empty($db_user))
                    $event->getForm()->get('email')
                        ->addError(new FormError("user.email.unique"));
            })
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($encoder) {
                $user = $event->getData();
                $suser = new SecurityUser($user);
                $user->setPassword($encoder->encodePassword($user->getPassword(), $suser->getSalt()));
            });
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PokeSphereBundle\Entity\User',
            'csrf_protection' => false,
            'validation_groups' => "registration"
        ));
    }
    public function getBlockPrefix()
    {
        return '';
    }
}
