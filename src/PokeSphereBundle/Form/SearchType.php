<?php
namespace PokeSphereBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 3/23/2017
 * Time: 8:18 AM
 */
class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('search', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Type your pokemon'
                )
            ))
        ;
    }
}
