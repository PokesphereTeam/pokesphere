<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 4/6/2017
 * Time: 10:05 AM
 */
namespace PokeSphereBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
class FriendCodeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fc1', TextType::class, ["required" => false])
            ->add('fc2', TextType::class, ["required" => false])
            ->add('fc3', TextType::class, ["required" => false])
            ->addModelTransformer(new CallbackTransformer(
                function ($data) {
                    if($data) {
                        $data = str_split($data, 4);
                        $result = [];
                        foreach ($data as $key => $fc)
                            $result["fc" . ($key + 1)] = $fc;
                        return $result;
                    }
                },
                function ($data) {
                    $new_ca = $data['fc1'].$data['fc2'].$data['fc3'];
                    return $new_ca == ""? null : $new_ca;
                }
            ));
    }
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        return $form->get('fc1')->getData().$form->get('fc2')->getData().$form->get('fc3')->getData();
    }
    public function getBlockPrefix()
    {
        return "friend_code";
    }
}
