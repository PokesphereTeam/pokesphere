<?php
namespace PokeSphereBundle\Form;
use GraphAware\Neo4j\OGM\EntityManager;
use PokeSphereBundle\Entity\GameVersion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
class GameType extends AbstractType
{
    /** @var  EntityManager */
    private $em;
    public function __construct(EntityManager $neo4j)
    {
        $this->em = $neo4j;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $game_versions = $this->em->getRepository(GameVersion::class)->findAll();
        $choices = [];
        foreach ($game_versions as $gv)
            $choices[$gv->getId()] = $gv;
        $builder
            ->add('pseudo')
            ->add('gameVersion',ChoiceType::class,[
                'choices' => $choices,
                'choice_value' => function($game) {
                    return $game?$game->getId():null;
                }
            ])
            ->add('comment');
    }
    public function getBlockPrefix()
    {
        return "";
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PokeSphereBundle\Entity\Game',
            'csrf_protection' => false
        ));
    }
}
