<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 4/10/2017
 * Time: 9:01 AM
 */
namespace PokeSphereBundle\Form;
use GraphAware\Neo4j\OGM\EntityManager;
use PokeSphereBundle\Entity\Avatar;
use PokeSphereBundle\Entity\User;
use PokeSphereBundle\Repository\AvatarRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
class DetailsType extends AbstractType
{
    /**
     * @var AvatarRepository
     */
    private $repo_avatar;
    public function __construct(EntityManager $neo4j)
    {
        $this->repo_avatar = $neo4j->getRepository(Avatar::class);
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $data_user */
        $data_user = $options['data'];
        $avatars_available = $this->repo_avatar->getAvalaibleUserAvatars($data_user->getId());
        $a_choices = [];
        foreach ($avatars_available as $avatar)
            $a_choices[$avatar->getId()] = $avatar;
        $current_year = date('Y');
        $builder
            ->add('email', EmailType::class)
            ->add('friendCode', FriendCodeType::class, ["required" => true])
            ->add('dateOfBirth',BirthdayType::class,[
                'format' => 'dd/MM/yyyy',
                'years' => range($current_year-10,$current_year-80)
            ])
            ->add('description')
            ->add('avatar',ChoiceType::class,[
                'choices' => $a_choices,
                'choice_value' => function($avatar) {
                    return $avatar?$avatar->getId():null;
                }
            ]);
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PokeSphereBundle\Entity\User',
            'csrf_protection' => false,
            'validation_groups' => "details"
        ));
    }
    public function getBlockPrefix()
    {
        return '';
    }
}
