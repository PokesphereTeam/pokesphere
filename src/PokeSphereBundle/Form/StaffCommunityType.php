<?php
/**
 * Created by PhpStorm.
 * User: Sobraz
 * Date: 08/05/2017
 * Time: 18:52
 */
namespace PokeSphereBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
class StaffCommunityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('user_id', NumberType::class);
    }
}
