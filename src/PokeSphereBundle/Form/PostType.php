<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 5/13/2017
 * Time: 1:08 PM
 */
namespace PokeSphereBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('content')
            ->add('public',CheckboxType::class);
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PokeSphereBundle\Entity\Post',
            'csrf_protection' => false
        ));
    }
    public function getBlockPrefix()
    {
        return "";
    }
}
