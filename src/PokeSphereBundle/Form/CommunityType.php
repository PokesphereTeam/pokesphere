<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 5/7/2017
 * Time: 10:38 PM
 */
namespace PokeSphereBundle\Form;
use GraphAware\Neo4j\OGM\EntityManager;
use PokeSphereBundle\Entity\Avatar;
use PokeSphereBundle\Entity\Community;
use PokeSphereBundle\Repository\AvatarRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
class CommunityType extends AbstractType
{
    /** @var AvatarRepository  */
    private $repo_avatar;
    /** @var \GraphAware\Neo4j\OGM\Repository\BaseRepository  */
    private $repo_commu;
    public function __construct(EntityManager $neo4j)
    {
        $this->repo_avatar = $neo4j->getRepository(Avatar::class);
        $this->repo_commu = $neo4j->getRepository(Community::class);
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Community $data_user */
        $community = isset($options['data'])?$options['data']:null;
        $avatars_available = $this->repo_avatar->getAvalaibleCommunityAvatars($community);
        $a_choices = [];
        foreach ($avatars_available as $avatar)
            $a_choices[$avatar->getId()] = $avatar;
        $builder
            ->add('name')
            ->add('description')
            ->add('avatar', ChoiceType::class, [
                'choices' => $a_choices,
                'choice_value' => function(Avatar $avatar) {
                    return $avatar ? $avatar->getId() : null;
                }
            ])
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                $pseudo = $event->getForm()->get('name')->getData();
                $db_user = $this->repo_commu->findBy(['name' => $pseudo]);
                if (!empty($db_user))
                    $event->getForm()->get('name')
                        ->addError(new FormError("community.name.unique"));
            });
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PokeSphereBundle\Entity\Community',
            'validation_groups' => "details",
            'csrf_protection' => false
        ));
    }
    public function getBlockPrefix()
    {
        return "";
    }
}
