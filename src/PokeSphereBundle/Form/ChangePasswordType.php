<?php
namespace PokeSphereBundle\Form;
use PokeSphereBundle\Entity\SecurityUser;
use PokeSphereBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
class ChangePasswordType extends AbstractType
{
    /** @var EncoderFactory $encoder */
    private $encoder;
    private $actual_password;
    public function __construct(EncoderFactory $encoder)
    {
        $this->encoder = $encoder->getEncoder(new User());;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $encoder = $this->encoder;
        $this->actual_password = $options['data']->getPassword();
        $builder
            ->add('actualPassword', PasswordType::class, [
                'mapped' => false
            ])
            ->add('password', RepeatedType::class, [
            'type' => PasswordType::class,
            'required' => true,
            'first_options'  => array('label' => 'Mot de passe'),
            'second_options' => array('label' => 'Confirmation')
        ])
            ->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event) use ($encoder) {
                $user = $event->getData();
                $suser = new SecurityUser($user);
                $actual_field = $event->getForm()->get('actualPassword')->getData();
                if(!$encoder->isPasswordValid($this->actual_password, $actual_field,$suser->getSalt()))
                    $event->getForm()->get('actualPassword')
                        ->addError(new FormError("user.pasword.incorrect"));
                else
                    $user->setPassword($encoder->encodePassword($user->getPassword(), $suser->getSalt()));
            });
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => "changePassword",
            'csrf_protection' => false
        ));
    }
    public function getBlockPrefix()
    {
        return '';
    }
}
