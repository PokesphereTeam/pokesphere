<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 6/2/2017
 * Time: 11:35 AM
 */
namespace PokeSphereBundle\Form;
use PokeSphereBundle\Entity\SecurityUser;
use PokeSphereBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
class UpdateRequestPasswordUpdateType extends AbstractType
{
    /** @var EncoderFactory $encoder */
    private $encoder;
    public function __construct(EncoderFactory $encoder)
    {
        $this->encoder = $encoder->getEncoder(new User());;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $encoder = $this->encoder;
        $builder
            ->add('email', EmailType::class)
            ->add('token', TextType::class, ['mapped' => false])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'required' => true,
                'first_options'  => array('label' => 'Mot de passe'),
                'second_options' => array('label' => 'Confirmation')
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event) use ($encoder) {
                $user = $event->getData();
                $suser = new SecurityUser($user);
                $user->setPassword($encoder->encodePassword($user->getPassword(), $suser->getSalt()));
            });
    }
    public function getBlockPrefix()
    {
        return "";
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false
        ));
    }
}
