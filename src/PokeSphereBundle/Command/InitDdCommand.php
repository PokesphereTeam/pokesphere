<?php
namespace PokeSphereBundle\Command;
use PokeSphereBundle\Entity\Avatar;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
class InitDdCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this->setName("initdb");
        $this->setDescription("populate db with inits information");
    }
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("populate avatars");
        $this->initAvatars();
    }
    private function initAvatars()
    {
        $em = $this->getContainer()->get('neo4j');
        $avatar_default = new Avatar();
        $avatar_default->setUrl(Avatar::BASE_FOLDER_AVATARS_USERS."/metamorph_pink.png");
        $avatar_default->setIsAvatarUser(true);
        $avatar_default->setTag("default");
        $em->persist($avatar_default);
        $blue_meta = new Avatar();
        $blue_meta->setUrl(Avatar::BASE_FOLDER_AVATARS_COMMUNITIES."/metamorph_blue.png");
        $blue_meta->setIsAvatarCommunity(true);
        $avatar_default->setTag("default");
        $em->persist($blue_meta);
        $em->flush();
    }
}
