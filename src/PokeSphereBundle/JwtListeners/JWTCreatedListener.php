<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 6/1/2017
 * Time: 1:58 PM
 */
namespace PokeSphereBundle\JwtListeners;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
class JWTCreatedListener
{
    /**
     * @param JWTCreatedEvent $event
     *
     * @return void
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        $expiration = new \DateTime('+1 day');
        $payload        = $event->getData();
        $payload['exp'] = $expiration->getTimestamp();
        $event->setData($payload);
    }
}
