<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 6/1/2017
 * Time: 11:16 AM
 */
namespace PokeSphereBundle\JwtListeners;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use PokeSphereBundle\Entity\SecurityUser;
use PokeSphereBundle\Service\ObjectToJson\DataSerializer;
class AuthenticationSuccessListener
{
    /** @var DataSerializer $serializer */
    private $serializer;
    public function __construct(DataSerializer $serializer)
    {
        $this->serializer = $serializer;
    }
    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();
        if (!$user instanceof SecurityUser) {
            return;
        }
        $data['user'] = $this->serializer->serializeData($user,['details','avatarInfo']);
        $event->setData($data);
    }
}
