<?php
/**
 * Created by PhpStorm.
 * User: rcollas
 * Date: 6/1/2017
 * Time: 12:12 PM
 */
namespace PokeSphereBundle\JwtListeners;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
class AuthenticationFailureListener
{
    /**
     * @param AuthenticationFailureEvent $event
     */
    public function onAuthenticationFailureResponse(AuthenticationFailureEvent $event)
    {
        $response = new JWTAuthenticationFailureResponse($event->getException()->getMessage());
        $event->setResponse($response);
    }
}
