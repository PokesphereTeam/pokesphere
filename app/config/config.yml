imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }
    - { resource: services_listeners.yml }
    - { resource: services_form.yml }
    - { resource: services_jwt.yml }
    - { resource: services_voters.yml }

# Put parameters here that don't need to change on each machine where the app is deployed
# http://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: en

framework:
    #esi: ~
    translator: { fallbacks: ['%locale%'] }
    secret: '%secret%'
    router:
        resource: '%kernel.root_dir%/config/routing.yml'
        strict_requirements: ~
    form: ~
    csrf_protection: ~
    validation: { enable_annotations: true }
    serializer: { enable_annotations: true }
    templating:
        engines: ['twig']
    default_locale: '%locale%'
    trusted_hosts: ~
    session:
        handler_id:  session.handler.neo4j
    fragments: ~
    http_method_override: true
    assets: ~
    php_errors:
        log: true

# Twig Configuration
twig:
    debug: '%kernel.debug%'
    strict_variables: '%kernel.debug%'
    form_themes:
            - 'form/friend_code.html.twig'

# Swiftmailer Configuration
swiftmailer:
    transport: '%mailer_transport%'
    host: '%mailer_host%'
    username: '%mailer_user%'
    password: '%mailer_password%'
    spool: { type: memory }
    auth_mode: login
    encryption: ssl
    
neo4j:
  profiling:
    enabled: true
  connections:
    default:
      schema: '%neo4j_proto%' # default (must be either "http" or "bolt")
      host: '%neo4j_host%' # default
      port: '%neo4j_port%' # optional, will be set to the proper driver's default port if not provided
      username: '%neo4j_username%' # default
      password: '%neo4j_password%' # default
  clients:
    default:
      connections: [default]
  entity_managers:
    default:
      client: default # defaults to "default"
      cache_dir: "%kernel.cache_dir%/neo4j" # defaults to system cache

nelmio_cors:
  defaults:
    allow_credentials: true
    allow_origin: ['*']
    allow_headers: ['*']
    allow_methods: ['*']
    expose_headers: []
    max_age: 0
    hosts: []
    origin_regex: false
  paths:
    '^/':
      allow_origin: ['*']
      allow_headers: ['*']
      allow_methods: ['POST', 'PUT', 'GET', 'DELETE', 'PATCH']
      max_age: 3600

lexik_jwt_authentication:
    private_key_path: '%jwt_private_key_path%'
    public_key_path:  '%jwt_public_key_path%'
    pass_phrase:      '%jwt_key_pass_phrase%'
    token_ttl:        '%jwt_token_ttl%'
    token_extractors:
      authorization_header:      # look for a token as Authorization Header
          enabled: false
          prefix:  Bearer
          name:    Authorization
      cookie:                    # check token in a cookie
          enabled: true
          name:    BEARER
      query_parameter:           # check token in query string parameter
          enabled: false
          name:    bearer